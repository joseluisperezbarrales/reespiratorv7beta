/*
  Modulo: Corazo.cpp

      Crea un reloj con DIVERSOS contadores
      Cada uno tiene su periodo diferente

      Necesario para simular el sistema

          void RelojExterno::Reset( void )
          void RelojExterno::Late( void )
 */
#include  "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;


//   Los flags los activamos para empezar
void RelojExterno::Reset( void )
{
  frecsistema         = 0;

  frecBarraSup        = 0;
  frecBarraInf        = 0;
  frecTeclado         = 0;

  flagBarraSup        = SI;
  flagBarraInf        = SI;
  flagTeclado         = SI;

  lcd2004.miMaquinaEstado.miRegistroEstado.recibidoNumAlarmas = MSG_SINCRONISMO_ON;
  lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraCon   = MSG_SINCRONISMO_ON;
  lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraAct   = MSG_SINCRONISMO_ON;
  lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraEdc   = MSG_SINCRONISMO_ON;
}


//  Genera un pulso en cada ciclo de loop
//  Se activan los pulsos de sincronismo
void RelojExterno::Late( void )
{
  //  Actualiza contadores de sistema

  Pulso();

  //  Actualiza flags cada uno con la frecuencia teorica
  //    Cuarzo Sistema
  // Inicio ciclo respiratorio
  if( frecsistema >= XTAL_EXTERNO1  )
  {
    Reset();
    //Serial.println("------------------------->");
    //    Ojo ! Desactivar esto en la Release version
    //    Esto debe hacerse externamente
    //      Nota: Si no se controla el estado HID_ESTADO_RESETLOOP
    //      al iniciar un ciclo respiratorio se salta PantallaBienvenida()
    //      ya que se activa flagexterno
    if( lcd2004.HidGet() != HID_ESTADO_RESETLOOP )
        lcd2004.flagexterno = SI;
  }

  //    Cuarzo Barra Superior
  if( frecBarraSup == TIME_BLINKBARRASUP )
  {
    flagBarraSup= SI;
    lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraSup = MSG_SINCRONISMO_OFF;
    //Serial.println("Reloj Barra Superior  Blink!!!! ------------------->");
  }
  else if( frecBarraSup >= XTAL_BARRASUP )
  {
    frecBarraSup =  0;
    flagBarraSup = SI;
    lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraSup = MSG_SINCRONISMO_ON;
    //Serial.println("Reloj Barra Superior  Inicio ------------------->");
  }

  //    Cuarzo Barra Inferior
  if( frecBarraInf == TIME_BLINKBARRAINF )
  {
    flagBarraInf = SI;
    lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraInf = MSG_SINCRONISMO_OFF;
    //Serial.println("Reloj Barra Inferior  Blink!!!! ------------------->");
  }
  else if( frecBarraInf >= XTAL_BARRAINF )
  {
    frecBarraInf =  0;
    flagBarraInf = SI;
    lcd2004.miMaquinaEstado.miRegistroEstado.recibidoBarraInf = MSG_SINCRONISMO_ON;
    //Serial.println("Reloj Barra Inferior  Inicio ------------------->");
  }


  //    Cuarzo Teclado
  if( frecTeclado >= XTAL_TECLADO )
  {
    frecTeclado =  0;
    flagTeclado = SI;
    lcd2004.miBotonera.Reset();
    //Serial.println("Reloj Teclado   -----    ----->");
  }

  //  Controla el tren de impulsos generado para leer el Encoder
  if( flagTeclado )
  {
    if( contpulsosTeclado > TECLADO_TRENPULSOS )
    {
      contpulsosTeclado=0;
      flagTeclado = NO;
    }
    else
        contpulsosTeclado++;
  }
}




//  Incrementa los contadores
void RelojExterno::Pulso( void )
{
  //    Necesarias para Interfaz con biblioteca TimerOne
  //      No empleadas en el programa salvo en Tracer()
  contadorticks++;
  flagticks=SI;

  //  Contador de los cristales internos de miRelojExterno ( cuarzos digitales )
  frecsistema++;
  frecTeclado++;
  frecBarraSup++;
  frecBarraInf++;
}

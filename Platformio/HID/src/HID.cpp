/*
     Modulo: HID.cpp
     Release 1.0

     Contiene los metodos de la clase

        void Hid::Setup( void )
        int Hid::HidSet( char estado )
        int Hid::HidGet( void )
        void Hid::HidMain( void )

        ToDo:
          Encapsular
          Reordenar metodos:
              int ModificaValor( int );
 */
#include  "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;




/*
    Metodo: Setup( void )

*/
void Hid::Setup()
{
  Serial.begin( BAUDIOS );

  //     Iniciamos el hardware asociado
  pinMode ( ENCODER_DT , INPUT );
  pinMode ( ENCODER_CLK , INPUT );
  pinMode ( SWITCH_RPM , INPUT_PULLUP);
  pinMode ( SWITCH_PICO, INPUT_PULLUP);
  pinMode ( SWITCH_PEEP , INPUT_PULLUP);
  pinMode ( SWITCH_ALARMAS , INPUT_PULLUP);

  //  Inicia LCD e Imprime pantalla Inicial
  pantalla.Setup();

  //  Empleado localmente con el shield HispalisRobIoTics Arduino Trainer
  pinMode ( 3 , OUTPUT );
  pinMode ( 5 , OUTPUT );
  pinMode ( 6 , OUTPUT );

}



/*
    Metodo: HidSet( void )

*/
int Hid::HidSet( int estado )
{
    switch( estado )
    {
      case HID_ESTADO_ON :
        flagexterno = SI;
        miMaquinaEstado.SetEstado( HID_ESTADO_ON );
      break;

      case HID_ESTADO_RESET :
        miMaquinaEstado.SetEstado( HID_ESTADO_RESET );
      break;

      case HID_ESTADO_PAUSE:
        flagexterno = NO;
        miMaquinaEstado.SetEstado( HID_ESTADO_PAUSE );
      break;

      case HID_ESTADO_STOP:
        flagexterno = NO;
        miMaquinaEstado.SetEstado( HID_ESTADO_STOP );
      break;

      case HID_ESTADO_SINCRONISMO_EXTERNO:

      miMaquinaEstado.SetEstado( HID_ESTADO_SINCRONISMO_EXTERNO );
      break;

      default:
        Serial.println("Error de Hid.Setup() ");
        return -1;
    }
    return ( miMaquinaEstado.GetEstado() );
}



/*
    Metodo: HidGet( void )

*/
int Hid::HidGet( void )
{
  return miMaquinaEstado.GetEstado();
}



/*
    Metodo: HidMain()

*/
void Hid::HidMain( void )
{
    //  Genera los pulsos internos generrados por el objeto miRelojInterno
    //  Se tiene que modificar al implantarla en el Sistema
    //  ya que se usa un timer externo de 1 msg
    miMaquinaEstado.miRegistroEstado.miRelojInterno.Late();

  //  Sincronismo:  Inicio de ciclo respiratorio --> A formar !
  //  flagexterno debe activarse externamente segun especificaciones
  //  y anularse de RelojExterno::Late()
  if( flagexterno )
  {
      GetConsignas();
      HidSet( HID_ESTADO_SINCRONISMO_EXTERNO );
      flagexterno = NO;
  }
  //  Rutina Principal
  miMaquinaEstado.RefrescaEstado();
}





/*
    Metodo: GetConsignas

    Simula las consignas de MechVentilator
    Los valores se proporcionan al sistema por via asincrona,
    al inicio de cada ciclo respiratorio ( unos 7sg de media )
*/
void Hid::GetConsignas( void )
{
  misConsignas.volumenminimo          =   misDatosExternos.volumenminimo;
  misConsignas.volumenmaximo          =   misDatosExternos.volumenmaximo;
  misConsignas.volumenconsigna        =   misDatosExternos.volumenconsigna;
  misConsignas.volumenactual          =   misDatosExternos.volumenactual;

  misConsignas.presionPICOminimo      =   misDatosExternos.presionPICOminimo;
  misConsignas.presionPICOmaximo      =   misDatosExternos.presionPICOmaximo;
  misConsignas.presionPICOconsigna    =   misDatosExternos.presionPICOconsigna;
  misConsignas.presionPICOactual      =   misDatosExternos.presionPICOactual;

  misConsignas.presionPEEPminimo      =   misDatosExternos.presionPEEPminimo;
  misConsignas.presionPEEPmaximo      =   misDatosExternos.presionPEEPmaximo;
  misConsignas.presionPEEPconsigna    =   misDatosExternos.presionPEEPconsigna;
  misConsignas.presionPEEPactual      =   misDatosExternos.presionPEEPactual;

  misConsignas.frecRPMminimo          =   misDatosExternos.frecRPMminimo;
  misConsignas.frecRPMmaximo          =   misDatosExternos.frecRPMmaximo;
  misConsignas.frecRPMconsigna        =   misDatosExternos.frecRPMconsigna;
  misConsignas.frecRPMactual          =   misDatosExternos.frecRPMactual;

  misConsignas.valorTRIGGER          =   misDatosExternos.valorTRIGGER;
  misConsignas.bateriaLOW             =   misDatosExternos.bateriaLOW;

  //    Flags de ALARMA
  misConsignas.flagAlarmaRPM          =  misDatosExternos.flagAlarmaRPM;
  misConsignas.flagAlarmaPICO         =  misDatosExternos.flagAlarmaPEEP;
  misConsignas.flagAlarmaPEEP         =  misDatosExternos.flagAlarmaPICO;

  misConsignas.flagAlarmaRPMBajo      = misDatosExternos.flagAlarmaRPMBajo;
  misConsignas.flagAlarmaRPMAlto      = misDatosExternos.flagAlarmaRPMAlto;
  misConsignas.flagAlarmaPICOBajo     = misDatosExternos.flagAlarmaPICOBajo;
  misConsignas.flagAlarmaPICOAlto     = misDatosExternos.flagAlarmaPICOAlto;
  misConsignas.flagAlarmaPEEPAlto     = misDatosExternos.flagAlarmaPEEPAlto;
  misConsignas.flagAlarmaPEEPBajo     = misDatosExternos.flagAlarmaPEEPBajo;

  //  Actualizamos miRegistroEstado.
  miMaquinaEstado.miRegistroEstado.AlarmaRPM  =  misDatosExternos.flagAlarmaRPMBajo +
                                                 misDatosExternos.flagAlarmaRPMAlto;

  miMaquinaEstado.miRegistroEstado.AlarmaPICO =  misDatosExternos.flagAlarmaPICOBajo +
                                                 misDatosExternos.flagAlarmaPICOAlto;

  miMaquinaEstado.miRegistroEstado.AlarmaPEEP =  misDatosExternos.flagAlarmaPEEPBajo +
                                                 misDatosExternos.flagAlarmaPEEPAlto;

  miMaquinaEstado.miRegistroEstado.AlarmaTRIG =  misDatosExternos.valorTRIGGER;

  //  Numero Total de alarmas
  miMaquinaEstado.miRegistroEstado.alarmasactivas = '0'
                                                        + misDatosExternos.flagAlarmaRPM
                                                        + misDatosExternos.flagAlarmaPEEP
                                                        + misDatosExternos.flagAlarmaPICO
                                                        + misDatosExternos.flagAlarmaRPMBajo
                                                        + misDatosExternos.flagAlarmaRPMAlto
                                                        + misDatosExternos.flagAlarmaPICOBajo
                                                        + misDatosExternos.flagAlarmaPICOAlto
                                                        + misDatosExternos.flagAlarmaPEEPAlto
                                                        + misDatosExternos.flagAlarmaPEEPBajo;
}

/*
     Modulo: MaquinaEstado.cpp

     Contiene los metodos de la clases

     void MaquinaEstado::Reset()
     int  MaquinaEstado::GetEstado()
     int MaquinaEstado::SetEstado( unsigned char estado )
     void MaquinaEstado::Loop()
     void MaquinaEstado::RefrescaEstado( void )

     int MaquinaEstado::RefrescaNumAlarmas( void )
     void MaquinaEstado::RefrescaBarraSuperior( void )
     int MaquinaEstado::RefrescaBarraInferior( void )
     int MaquinaEstado::RefrescaPantallaEdicion( int pestado )
     int MaquinaEstado::RefrescaBarraConsignas( void )
     int MaquinaEstado::RefrescaBarraActuales( void )
     int MaquinaEstado::ParpadeaRotuloAlarma( void )

     void RegistroEstado::Reset( void )
 */

#include  "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;
extern String menuItems[MENU_ITEMS];
extern Mensaje texto[ARRAYTEXTOS_LEN];


/*
     Metodo:   Reset()

 */
void MaquinaEstado::Reset()
{
  miRegistroEstado.Reset();
  miRegistroEstado.miRelojInterno.Reset();
}



/*
     Metodo:   Reset()

 */
void RegistroEstado::Reset( void )
{
  alarmasactivas      =  0;
  AlarmaRPM           =  NO;
  AlarmaPICO          =  NO;
  AlarmaPEEP          =  NO;

  recibidoNumAlarmas  = MSG_SINCRONISMO_ON;
  recibidoBarraInf    = MSG_SINCRONISMO_ON;
  recibidoBarraSup    = MSG_SINCRONISMO_ON;
  recibidoBarraCon    = MSG_SINCRONISMO_ON;
  recibidoBarraAct    = MSG_SINCRONISMO_ON;
  recibidoBarraEdc    = MSG_SINCRONISMO_ON;
}



/*
     Metodo:   GetEstado()

 */
int  MaquinaEstado::GetEstado()
{
  return hidestadoactual;
}



/*
    Metodo:     SetEstado( char estado )

    Devuelve:
        -1 Error
*/
int MaquinaEstado::SetEstado( unsigned char estado )
{
  switch( estado )
  {
    case HID_ESTADO_ON:
      hidestadoactual = HID_ESTADO_ON;
    break;
    case HID_ESTADO_RESET:
      hidestadoactual = HID_ESTADO_RESET;
    break;
    case HID_ESTADO_PAUSE:
      hidestadoactual = HID_ESTADO_PAUSE;
      break;
    case( HID_ESTADO_STOP ):
      hidestadoactual = HID_ESTADO_STOP;
    break;
    default:
      hidestadoactual = HID_ESTADO_STOP;
    break;
    case HID_ESTADO_SINCRONISMO_EXTERNO:
     hidestadoactual = HID_ESTADO_SINCRONISMO_EXTERNO;
    break;
  }
  return hidestadoactual;
}



/*
  Metodo: Loop()
    Acciones asociadas al estado HID_ESTADO_LOOP

      -   Actualiza la fila 0 de alarmas rotatoria
      -   Refresca la barra de switches en la fila 4
      -   Actualiza el parpadeo del switch Alarma / Vol
      -   Refresca la pantalla de consignas ( fila 2 )
      -   Lee la Botonera y captura los cambios de estado por Teclado
      -   Activa el Encoder
*/
void MaquinaEstado::Loop()
{
  static unsigned char estadoaux;
  //    Mofeta();                           //  Usado con el shield Arduino Trainer
  //    Actualiza Barra Inferior
  if( miRegistroEstado.miRelojInterno.flagBarraInf )
  {
    if( ParpadeaRotuloAlarma() )
      miRegistroEstado.miRelojInterno.flagBarraInf = NO;
  }
  //    Actualiza la Barra Superior de Alarmas
  else if( miRegistroEstado.miRelojInterno.flagBarraSup )
    RefrescaBarraSuperior();

  //    Actualiza la Fila 1 del LCD
  if( miRegistroEstado.miRelojInterno.flagConsignas )
  {
      if( RefrescaBarraConsignas() )
        miRegistroEstado.miRelojInterno.flagConsignas  = NO;
  }
  else
  {
    if( miRegistroEstado.miRelojInterno.flagTeclado )
    {
        //    Lee el Teclado
        if( lcd2004.miBotonera.MuestreaTeclado() )
        {
          //  estadoaux es el actual right now !
          estadoaux = lcd2004.miBotonera.GetEstado();
          if( estadoaux == hidestadoanterior )
          //  Por si se deja el dedo pulsado mucho tiempo
          {
            if( estadoaux != HID_ESTADO_SWITCHALARMAPULSADO )
                return;
          }

          hidestadoanterior = hidestadoactual;
          hidestadoactual = estadoaux;
          miRegistroEstado.miRelojInterno.flagTeclado   = NO;
          miRegistroEstado.miRelojInterno.flagEncoder   = SI;
          return;
        }
     }
     //    Modo Encoder
     else if( miRegistroEstado.miRelojInterno.flagEncoder )
     {
       switch( hidestadoanterior )
       {
         case HID_ESTADO_SWITCHRPMPULSADO:
         case HID_ESTADO_SWITCHPICOPULSADO:
         case HID_ESTADO_SWITCHPEEPPULSADO:

         case HID_ESTADO_MENU_ALARMAS:
         case HID_ESTADO_ALARMA_RPM_MINIMO:
         case HID_ESTADO_ALARMA_RPM_MAXIMO:
         case HID_ESTADO_ALARMA_PICO_MINIMO:
         case HID_ESTADO_ALARMA_PICO_MAXIMO:
         case HID_ESTADO_ALARMA_PEEP_MINIMO:
         case HID_ESTADO_ALARMA_PEEP_MAXIMO:
         case HID_ESTADO_ALARMA_TRIGGER:
           hidestadoactual = lcd2004.miBotonera.miEncoder.ModificaValor( hidestadoanterior );
         break;
         //   En caso de error
         default:
         hidestadoactual = HID_ESTADO_ENCODER_TIMEOUT;
        break;
      }
    }
  }
}



/*
  Metodo: RefrescaEstado()

    Gestiona las acciones a realizar en cada estado

    Llamada por HidMain()
    Retorna a loop()
*/
void MaquinaEstado::RefrescaEstado( void )
{
    switch( hidestadoactual )
    {
//  Recibimos datos de ciclo respiratorio
// Ciclo 0 , Casilla de Salida
      case HID_ESTADO_RESET:
        if( lcd2004.pantalla.PantallaBienvenida() )
        {
          hidestadoactual   = HID_ESTADO_RESETLOOP;
          //hidestadoactual = HID_ESTADO_ON;
        }
        else
          return;
      return;

//  Pantalla Bienvenida
      case HID_ESTADO_RESETLOOP:
        if( lcd2004.miBotonera.MuestreaTeclado() )
        {
          switch( lcd2004.miBotonera.GetEstado() )
          {
            case HID_ESTADO_SWITCHPEEPPULSADO:
                lcd2004.miBotonera.Reset();
                miRegistroEstado.miRelojInterno.flagTeclado = NO;
                hidestadoactual = HID_ESTADO_ON;
            break;
          }
        }
      break;
//  Ciclo 2
      case HID_ESTADO_ON:
        lcd2004.HidSet( HID_ESTADO_ON );
        //  Que activa   flagexterno = SI;
        //  Y por HidMain() se salta a HID_ESTADO_SINCRONISMO_EXTERNO
      break;

//    Sincronismo Inicial externo
//    Ciclo
      case HID_ESTADO_SINCRONISMO_EXTERNO:
          hidestadoactual   = HID_ESTADO_NUMALARMAS;
      break;
//    14  Ciclos
      case HID_ESTADO_NUMALARMAS:
          if( RefrescaNumAlarmas() )
            hidestadoactual = HID_ESTADO_PROCESACONSIGNAS;
      break;
//    Ciclo
      case HID_ESTADO_PROCESACONSIGNAS:
          if( lcd2004.pantalla.ConsignasConvierte() )
          {
            if( !lcd2004.pantalla.modoEdicion )
              miRegistroEstado.miRelojInterno.flagConsignas = SI;
            hidestadoactual = HID_ESTADO_IMPRIMECONSIGNAS;
          }
      break;
      case HID_ESTADO_IMPRIMECONSIGNAS:
      if( miRegistroEstado.miRelojInterno.flagConsignas )
      {
          if( RefrescaBarraConsignas() )
          {
            hidestadoactual = HID_ESTADO_IMPRIMEACTUALES;
            miRegistroEstado.miRelojInterno.flagConsignas  = NO;
          }
      }
      else
        hidestadoactual = HID_ESTADO_IMPRIMEACTUALES;
      break;
      case HID_ESTADO_IMPRIMEACTUALES:
          if( RefrescaBarraActuales() )
          {
            hidestadoactual = HID_ESTADO_IMPRIMEBARRAINF;
          }
      break;
      case HID_ESTADO_IMPRIMEBARRAINF:
          //  Se imprime "| RMP|PICO|PEEP"
          if( RefrescaBarraInferior() )
            hidestadoactual = HID_ESTADO_LOOP;
      break;

//      Bucle loop()
//  Ciclo 9 desde HID_ESTADO_SINCRONISMO_EXTERNO ??

      case HID_ESTADO_LOOP:
          Loop();
      break;
//      <--------   Fin Bucle Principal


//      Del Estado anterior se sale al pulsar uno de los switches
      case HID_ESTADO_SWITCHRPMPULSADO:
      case HID_ESTADO_SWITCHPICOPULSADO:
      case HID_ESTADO_SWITCHPEEPPULSADO:

      case HID_ESTADO_ALARMA_RPM_MINIMO:
      case HID_ESTADO_ALARMA_RPM_MAXIMO:
      case HID_ESTADO_ALARMA_PICO_MINIMO:
      case HID_ESTADO_ALARMA_PICO_MAXIMO:
      case HID_ESTADO_ALARMA_PEEP_MINIMO:
      case HID_ESTADO_ALARMA_PEEP_MAXIMO:
      case HID_ESTADO_ALARMA_TRIGGER:

        if( RefrescaPantallaEdicion( hidestadoactual ) )
        {
          lcd2004.miBotonera.miEncoder.Reset( hidestadoactual );
          lcd2004.pantalla.modoEdicion = SI;
          miRegistroEstado.miRelojInterno.flagTeclado = NO;
          hidestadoanterior = hidestadoactual;
          hidestadoactual   = HID_ESTADO_LOOP;
        }
      break;

      case HID_ESTADO_MENU_ALARMAS:
        //  Se accede en el ciclo posterior tras pulsar SWITCH_ALARMAS
        lcd2004.miBotonera.miEncoder.Reset( HID_ESTADO_MENU_ALARMAS );
        miRegistroEstado.miRelojInterno.flagTeclado = NO;
        hidestadoanterior = hidestadoactual;
        hidestadoactual   = HID_ESTADO_LOOP;
      break;

      //  Ojo que aqui se puede entrar hasta en tres casos diferentes
      //    Al pulsarlo para entrar en el Menu de seleccion de alarma
      //    Al pulsarlo para seleccionar una consigna del menu y EDITARLA
      //    Al pulsarlo para GRABARLA
      case HID_ESTADO_SWITCHALARMAPULSADO:
      {
          switch( lcd2004.miBotonera.miEncoder.switchconsigna)
          {
            case HID_ESTADO_SWITCHESOFF:
                //  Primera vz que se pulsa   OK
                //  Inicio de Menu()
                hidestadoactual = HID_ESTADO_MENU_ALARMAS;
            return;
            case HID_ESTADO_MENU_ALARMAS:
                // Segunda vez que se pulsa (  Salida de Menu()  )
                //  Obtenemos elemento de menu
                switch( lcd2004.miBotonera.miEncoder.EncoderGetCursor() )
                {
                  //    Las ctes 0,1,2.. son los estados del menu de Alarmas
                  //    ToDo: Asignar #define
                  case 0: hidestadoactual = HID_ESTADO_ALARMA_RPM_MINIMO;break;
                  case 1: hidestadoactual = HID_ESTADO_ALARMA_RPM_MAXIMO;break;
                  case 2: hidestadoactual = HID_ESTADO_ALARMA_PICO_MINIMO;break;
                  case 3: hidestadoactual = HID_ESTADO_ALARMA_PICO_MAXIMO;break;
                  case 4: hidestadoactual = HID_ESTADO_ALARMA_PEEP_MINIMO;break;
                  case 5: hidestadoactual = HID_ESTADO_ALARMA_PEEP_MAXIMO;break;
                  case 6: hidestadoactual = HID_ESTADO_ALARMA_TRIGGER;break;
                  case 7: hidestadoactual = HID_ESTADO_ENCODER_TIMEOUT;break;     //    Recruit
                }
                lcd2004.pantalla.modoEdicion = SI;
            return;
            //    Grabacion de consignas principales
            //      Primera  vez que se pulsa SWITCH_ALARMAS ( caso de consigna principal  )
            //      Tercera vez que se pulsa SWITCH_ALARMAS  ( caso de consigna secundaria  )
            case HID_ESTADO_SWITCHRPMPULSADO:
                lcd2004.misConsignas.frecRPMconsigna = lcd2004.miBotonera.miEncoder.EncoderGetActual();
                lcd2004.pantalla.ultraitoa( lcd2004.misConsignas.frecRPMconsigna  , &texto[SCREEN_MSG_CONSGN_RPM].cadena[0] );
            break;
            case HID_ESTADO_SWITCHPICOPULSADO:
                lcd2004.misConsignas.presionPICOconsigna = lcd2004.miBotonera.miEncoder.EncoderGetActual();
                lcd2004.pantalla.ultraitoa( lcd2004.misConsignas.presionPICOconsigna  , &texto[SCREEN_MSG_CONSGN_PICO].cadena[0] );
            break;
            case HID_ESTADO_SWITCHPEEPPULSADO:
                lcd2004.misConsignas.presionPEEPconsigna = lcd2004.miBotonera.miEncoder.EncoderGetActual();
                lcd2004.pantalla.ultraitoa( lcd2004.misConsignas.presionPEEPconsigna  , &texto[SCREEN_MSG_CONSGN_PEEP].cadena[0] );
            break;
            case HID_ESTADO_ALARMA_RPM_MINIMO:
               lcd2004.misConsignas.frecRPMminimo = lcd2004.miBotonera.miEncoder.EncoderGetActual();
            break;
            case HID_ESTADO_ALARMA_RPM_MAXIMO:
                lcd2004.misConsignas.frecRPMmaximo = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;
            case HID_ESTADO_ALARMA_PICO_MINIMO:
                lcd2004.misConsignas.presionPICOminimo = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;
            case HID_ESTADO_ALARMA_PICO_MAXIMO:
                lcd2004.misConsignas.presionPICOmaximo = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;
            case HID_ESTADO_ALARMA_PEEP_MINIMO:
                lcd2004.misConsignas.presionPEEPminimo = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;
            case HID_ESTADO_ALARMA_PEEP_MAXIMO:
                lcd2004.misConsignas.presionPEEPmaximo = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;
            case HID_ESTADO_ALARMA_TRIGGER:
                lcd2004.misConsignas.valorTRIGGER = lcd2004.miBotonera.miEncoder.EncoderGetConsigna();
            break;

            default:
                Serial.println("Error RefrescaEstado() case HID_ESTADO_SWITCHALARMAPULSADO");
                hidestadoactual  =  HID_ESTADO_ENCODER_TIMEOUT;
            break;
          }
          //    Tras grabar la consigna en la estructura, salimos
          hidestadoactual = HID_ESTADO_ENCODER_TIMEOUT;
      }
      break;

//      <--------   Fin Estados provocados por Botones pulsados

//      Modo Encoder

      //      Sañida General y puesta a cero
      case HID_ESTADO_ENCODER_TIMEOUT:
        //  Refresca inmediato Consignas
        digitalWrite( 6 , LOW );
        digitalWrite( 3,  LOW );
        digitalWrite( 5,  LOW );
        lcd2004.miBotonera.miEncoder.Reset( hidestadoactual );
        miRegistroEstado.miRelojInterno.flagBarraInf  = SI;
        miRegistroEstado.miRelojInterno.flagConsignas = SI;
        miRegistroEstado.miRelojInterno.flagEncoder   = NO;
        lcd2004.pantalla.modoEdicion                  = NO;
        hidestadoanterior = HID_ESTADO_SWITCHESOFF;
        hidestadoactual = HID_ESTADO_LOOP;
        break;

      case HID_ESTADO_ENCODER_REFRESCO:
        if( lcd2004.pantalla.RefrescaEncoder( hidestadoanterior ) )
            hidestadoactual = HID_ESTADO_LOOP;
      break;

//      <--------   Fin Estados en Modo Encoder


//   --------------------------------------------
      //    Para el sistema ( no sale de aqui )
      case HID_ESTADO_STOP:
      break;
    }
}
//      <---------------------------------
//          Fin FefrescaEstado()



/*
    Metodos de enlace con clase Screen

  1)  Para escribir en el LCD, la clase MaquinaEstado establece un protocolo de
    comunicacion enviando un mensaje ( un byte ) a la clase Screen
    La clase Screen devuelve un byte de status

    El protocolo se inicia con MSG_SINCRONISMO_ON
    y se finaliza con

  2)  MaquinaEstado envia                 Screen responde con

    MSG_SINCRONISMO_ON     <-------->      MSG_ON
    MSG_ON                 <-------->      MSG_O1
    MSG_O1                 <-------->      MSG_O2
    .....
    MSG_XX                 <-------->      MSG_FINPROCESO_ON

  3)  Cada metodo aqui tiene un metodo equivalente SetXXXX en clase Screen

    RefrescaNumAlarmas       <---->    SetNumAlarmas
    RefrescaBarraSuperior    <---->    SetBarraSuperior
    ..........

  4)  En los metodos Set de clase Screen se imprimen los mensajes en el LCD
      Dichos mensajes se tratan como cadenas de caracteres
      ( se declaran como Strings por rapidez de codigo; no precisa malloc() )

      Y los mensajes se imprimen 1 caracter cada vez
*/



int MaquinaEstado::RefrescaNumAlarmas( void )
{
  switch ( miRegistroEstado.recibidoNumAlarmas )
  {
    case MSG_SINCRONISMO_ON:
          //  Creamos el mensaje SCREEN_NUMALARMAS a partir de alarmasactivas
          texto[SCREEN_NUMALARMAS].cadena[0] =  miRegistroEstado.alarmasactivas;
          miRegistroEstado.recibidoNumAlarmas = lcd2004.pantalla.SetNumAlarmas( MSG_SINCRONISMO_ON );
    break;
    case MSG_ON:
        miRegistroEstado.recibidoNumAlarmas = lcd2004.pantalla.SetNumAlarmas( MSG_ON );
    break;
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoNumAlarmas = MSG_SINCRONISMO_ON;
    return SI;
    default:
        miRegistroEstado.recibidoNumAlarmas= lcd2004.pantalla.SetNumAlarmas( miRegistroEstado.recibidoNumAlarmas );
    break;
  }
  return NO;
}



void MaquinaEstado::RefrescaBarraSuperior( void )
{
  //    Protocolo de comunicacion entre clases MaquinaEstado y Screen
  switch( miRegistroEstado.recibidoBarraSup )
  {
    case MSG_SINCRONISMO_ON:
        miRegistroEstado.recibidoBarraSup = lcd2004.pantalla.SetBarraSuperior( MSG_SINCRONISMO_ON );

    break;
    case MSG_ON:
        miRegistroEstado.recibidoBarraSup = lcd2004.pantalla.SetBarraSuperior( MSG_ON );
    break;
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoBarraSup             = MSG_DESCONEXION;
        miRegistroEstado.miRelojInterno.flagBarraSup  = NO;
    break;
    case MSG_SINCRONISMO_OFF:
        miRegistroEstado.recibidoBarraSup = lcd2004.pantalla.SetBarraSuperior( MSG_SINCRONISMO_OFF );
    break;
    case MSG_OFF:
        miRegistroEstado.recibidoBarraSup = lcd2004.pantalla.SetBarraSuperior( MSG_OFF );
    break;
    case MSG_ERROR:
    case MSG_FINPROCESO_OFF:
        miRegistroEstado.recibidoBarraSup             = MSG_SINCRONISMO_OFF;
        miRegistroEstado.miRelojInterno.flagBarraSup  = NO;
    break;
  }
}



int MaquinaEstado::RefrescaBarraInferior( void )
{
  switch ( miRegistroEstado.recibidoBarraInf )
  {
    case MSG_SINCRONISMO_ON:
          miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetBarraInferior( MSG_SINCRONISMO_ON );
    break;
    case MSG_ON:
        miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetBarraInferior( MSG_ON );
    break;
    case MSG_ERROR:
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoBarraInf = MSG_SINCRONISMO_ON;
    return SI;
  }
  return NO;
}



int MaquinaEstado::RefrescaPantallaEdicion( int pestado )
{
  switch ( miRegistroEstado.recibidoBarraEdc )
  {
    case MSG_SINCRONISMO_ON:
          lcd2004.pantalla.SetEstadoEdicion( pestado );         //  Activamos campo estadoEdicion en clase Screen
          miRegistroEstado.recibidoBarraEdc = lcd2004.pantalla.SetPantallaEdicion( MSG_SINCRONISMO_ON );
    break;
    case MSG_ON:
        miRegistroEstado.recibidoBarraEdc = lcd2004.pantalla.SetPantallaEdicion( MSG_ON );
    break;
    case MSG_FINPROCESO_OFF:
        miRegistroEstado.recibidoBarraEdc = MSG_SINCRONISMO_ON;
    return SI;
    default:
        miRegistroEstado.recibidoBarraEdc = lcd2004.pantalla.SetPantallaEdicion( miRegistroEstado.recibidoBarraEdc );
    break;
  }
  return NO;
}



int MaquinaEstado::RefrescaBarraConsignas( void )
{
  switch( miRegistroEstado.recibidoBarraCon )
  {  //    Protocolo de comunicacion entre clases MaquinaEstado y Screen
    case MSG_SINCRONISMO_ON:
        miRegistroEstado.recibidoBarraCon = lcd2004.pantalla.SetBarraConsignas( MSG_SINCRONISMO_ON );
    break;
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoBarraCon = MSG_SINCRONISMO_ON;
        return SI;
    default:
       miRegistroEstado.recibidoBarraCon = lcd2004.pantalla.SetBarraConsignas( miRegistroEstado.recibidoBarraCon );
    break;
    case MSG_ERROR:
        Serial.println("Error en MaquinaEstado::RefrescaConsignas() ");
        hidestadoactual  =  HID_ESTADO_ENCODER_TIMEOUT;
    break;
  }
  return NO;
}



int MaquinaEstado::RefrescaBarraActuales( void )
{
  switch( miRegistroEstado.recibidoBarraAct )
  {  //    Protocolo de comunicacion entre clases MaquinaEstado y Screen
    case MSG_SINCRONISMO_ON:
        miRegistroEstado.recibidoBarraAct = lcd2004.pantalla.SetBarraActuales( MSG_SINCRONISMO_ON );
    break;
    case MSG_ON:
        miRegistroEstado.recibidoBarraAct = lcd2004.pantalla.SetBarraActuales( MSG_ON );
    break;
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoBarraAct = MSG_SINCRONISMO_ON;
        return SI;
    break;
    default:
       miRegistroEstado.recibidoBarraAct = lcd2004.pantalla.SetBarraActuales( miRegistroEstado.recibidoBarraAct );
    break;
  }
  return NO;
}



int MaquinaEstado::ParpadeaRotuloAlarma( void )
{
  switch ( miRegistroEstado.recibidoBarraInf )
  {
    case MSG_SINCRONISMO_ON:
          miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetRotuloAlarma( MSG_SINCRONISMO_ON );
    break;
    case MSG_ON:
        miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetRotuloAlarma( MSG_ON );
    break;
    case MSG_FINPROCESO_ON:
        miRegistroEstado.recibidoBarraInf             = MSG_DESCONEXION;
        miRegistroEstado.miRelojInterno.flagBarraInf  = NO;
    break;
    case MSG_SINCRONISMO_OFF:
        miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetRotuloAlarma( MSG_SINCRONISMO_OFF );
    break;
    case MSG_OFF:
        miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetRotuloAlarma( MSG_OFF );
    break;
    case MSG_FINPROCESO_OFF:
        miRegistroEstado.recibidoBarraInf             = MSG_SINCRONISMO_OFF;
        miRegistroEstado.miRelojInterno.flagBarraInf  = NO;
    return SI;
    default:
        miRegistroEstado.recibidoBarraInf = lcd2004.pantalla.SetRotuloAlarma( miRegistroEstado.recibidoBarraInf );
    break;
  }
  return NO;
}

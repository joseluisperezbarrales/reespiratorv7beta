/*
    Modulo: Mensajes.cpp

      Propuesta de Internacionalizacion
      Estos son los mensajes del sistema hacia el LCD

    NOTA: Se bosqueja como implantar los mensajes en memoria FLASH
    con PROGMEM

    (  Extraer una cadena con PROGMEM lleva de 8 a 12 usg )
*/

#include  "HID.h"
#include <avr/pgmspace.h>

extern Hid lcd2004;
extern class Consigna misDatosExternos;

/*
    NOTA:
            Propuesta de Internacionalizacion
            Estos son los mensajes del sistema hacia el LCD
            Extraer una cadena con PROGMEM lleva de 8 a 12 usg
*/
Mensaje texto[ARRAYTEXTOS_LEN]=
{
  //  2  Numero de alarmas
  {  0 ,  0 , "|("    ,                  },   //  0
  {  0 ,  3 , ")|"    ,                  },

  //  6   Barra de Menu Inferior
  {  3 ,  0 , "|RPM|PICO|PEEP| VOL|",   },   //  02
  {  0 ,  2 , "1"    ,                  },
  {  1 ,  0 , "|",                      },
  {  2 ,  0 , "|",                      },
  {  3 , 15 , " VOL|",                  },

  //    Variable qRefrescaConsignaue se está editando Actualmente
  //    Puede ser principal ( frecRPM, switchPICO, sitchPEEP )
  //    o secundaria ( cualquier valor minimo a maximo )
  //    Fila 1 --> Pantalla Edicion ( ACTUAL )
  {  1 ,  9 , "123",                    },      //  07
  //    Fila 1 --> Pantalla Edicion ( EN EDICION )
  {  1 , 16 , "123",                    },      //  08

  {  3 , 15 , "Alrs|",                   },

  //  2   Barra de Menu Superior
  {  0 ,  5 , " << ALARMA >>  ",        },   //  10
  {  0 ,  5 , "               ",        },

  // 6   Barra de Consignas
  {  1 ,  4 , "| ",                     },   //  12
  {  1 ,  9 , "| ",                     },
  {  1 , 14 , "| ",                     },
  {  1 , 15 , "trig",                   },
  {  1 , 15 , "    ",                   },
  {  1 , 19 , "|",                      },

  // 4   Barra de Actuales
  {  2 ,  4 , "| ",                     },   //  18
  {  2 ,  9 , "| ",                     },
  {  2 , 14 , "| ",                     },
  {  2 , 19 , "|",                      },

  //      Elementos de Pantalla Edicion
  //  4    Pantalla Edicion
  {  1 ,  1 , "RPM    (",              },    //  22
  {  1 ,  1 , "PICO   (",              },
  {  1 ,  1 , "PEEP   (",              },
  {  1 , 12 ,  ") -> ",                 },   //  25
  //  7    Alarmas
  {  1 , 1 ,  "Al rmp (",              },    //  26
  {  1 , 1 ,  "Al RMP (",              },
  {  1 , 1 ,  "Al pico(",              },
  {  1 , 1 ,  "Al PICO(",              },
  {  1 , 1 ,  "Al peep(",              },
  {  1 , 1 ,  "Al PEEP(",              },
  {  1 , 1 ,  "TRIGGER(",              },
  //      Fin de Elementos de Pantalla Edicion

  //  4   Pantalla Inicio
  {  0 , 2 ,  "RESPIRATOR 2020",        },   //  33
  {  1 , 0 ,  "de personas",            },
  {  2 , 5 ,  "para personas",          },
  {  3 , 1 ,  "v 001.065",              },

  //  4   Pantalla ON
  {  0 , 0 ,  "RESPIRATOR EN REPOSO",   },   //  37
  {  1 , 0 ,  "           ",            },
  {  2 , 5 ,  "             ",          },
  {  3 , 0 ,  "CONFIGURAR    marcha",   },

  //    Variables ( Consignas y actuales )
  //    Fila 2 --> Actuales
  {  2 ,  1 , "123*",                  },     //  41-->  RPM
  {  2 ,  6 , "123*",                  },     //  PICO
  {  2 , 11 , "123*",                  },     //  PEEP
  {  2 , 16 , "123*",                  },     //  VOL

  //    Fila 1 --> Consignas
  {  1 ,  1 , "123",                   },      //  PICO
  {  1 ,  6 , "123",                   },      //  PICO
  {  1 , 11 , "123",                   },      //  PEEP

  //  Edicion Consigna Trigger
  {  1 ,  9 , "ON ",                    },      //  48
  {  1 ,  9 , "OFF",                    },      //  49
  {  1 , 16 , "ON ",                    },      //  50
  {  1 , 16 , "OFF",                    },      //  51

};





//    <-----------------
//    Para aquel que desee incluir en memoria FLASH el vector anterior
//
//  En desarrollo
//  De aqui al final del presente fichero el codigo no se emplea

const char string_0[] PROGMEM = "String 0"; // "String 0" etc are strings to store - change to suit.
const char string_1[] PROGMEM = "String 1";
const char string_2[] PROGMEM = "String 2";
const char string_3[] PROGMEM = "String 3";
const char string_4[] PROGMEM = "String 4";
const char string_5[] PROGMEM = "String 5";


// Then set up a table to refer to your strings.
const char *const string_table[] PROGMEM = {string_0, string_1, string_2, string_3, string_4, string_5};
char buffer[30];


//  Documentar
void TestProgMem()
{
  long anterior , actual , diferencia;

  int i;

  for (i = 0; i < 6; i++)
  {
    anterior = micros();
    strcpy_P(buffer, (char *)pgm_read_word(&(string_table[i])));
    actual = micros();
    Serial.print(buffer);

    diferencia = actual - anterior;
    Serial.print("\tDiferencia ..");Serial.println(diferencia);
  }

  for (i = 0 ; i < ARRAYTEXTOS_LEN ; i++)
    {
      anterior = micros();
      actual = micros();
      diferencia = actual - anterior;
      Serial.print("Con variables -->Diferencia ..");Serial.println(diferencia);

      Serial.print("I...");Serial.print( i);
      delay(100);
      Serial.print("\t\tSize(bytes) ..");Serial.print( sizeof( texto[i] ) );
      delay(100);
      Serial.print(" Valor -->[*");Serial.print( texto[i].cadena);Serial.print("*]");
      Serial.print("\t   Fil ");Serial.print( texto[i].fil);
      Serial.print("\t Col ");Serial.println( texto[i].col);
    }

  Serial.println("____________________________________________________");
  Serial.print("Tamano Array ..");Serial.println( sizeof( texto ) );
      delay(100);
  for(;;);
}

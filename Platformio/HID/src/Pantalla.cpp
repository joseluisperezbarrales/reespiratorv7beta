/*
     Modulo: Pantalla.cpp

     Contiene los metodos de la clase

        void Screen::GoTo( int col , int fil )
        void Screen::PrintMensaje( int numero )
        int Screen::Serializa( String item )

        int Screen:: RefrescaConsigna( int valor )
        int Screen:: RefrescaTrigger( int valor )
        int Screen:: RefrescaValorEnEdicion( int valor )
        int Screen:: RefrescaTriggerenEdicion( int valor )

        void Screen:: RefrescaEncoder( void )
        int Screen::ConsignasConvierte( void )
        int Screen:: ItoaLotes( void )
        void Screen::ultraitoa( int valor , char *salida )

        void Screen::ExternalInput( void )
        void Screen::Setup()
        void Screen::PantallaBienvenida()

        void Screen::NumAlarmasMain( void )
        void Screen:: RefrescaConsigna( int valor )

        unsigned char Screen::SetRotuloAlarma( unsigned char mensaje )
        unsigned char Screen::SetBarraInferior( unsigned char mensaje )
        unsigned char Screen::SetBarraSuperior( unsigned char mensaje )
        unsigned char Screen::SetBarraConsignas( unsigned char mensaje )
        unsigned char Screen::SetPantallaEdicion( unsigned char mensaje )
 */
#include  "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;
extern String menuItems[MENU_ITEMS];
extern Mensaje texto[ARRAYTEXTOS_LEN];


/*
  Metodo: Goto( col , fil )

  Interfaz con funciones de bajo nivel ( primitivas )
*/
void Screen::GoTo( int col , int fil )
{
  LCD.setCursor(  col , fil );
}


int Screen::PrintMensaje( int numero )

{
  static int cont = NO, fastcol , fastfil;
  static char * caracter;

  if( !cont )                      //   Llamada Inicial
  {
    caracter = &texto[numero].cadena[0];
    fastcol = texto[numero].col;
    fastfil = texto[numero].fil;
    cont++;
    GoTo( fastcol , fastfil );
    return NO;
  }
  else if( *caracter )            // Caracter actual a imprimir no nulo
  {
    //GoTo( fastcol , fastfil );
    LCD.print( *caracter);

    //    Permite ver la salida por el monitor Serie
    #ifdef SALIDAPANTALLA
      Serial.println( *caracter );
    #endif

    caracter++;
    cont++;
    fastcol++;
    return NO;
  }
  else                            //   Fin de la cadena
  {
    cont = NO;
    return SI;
  }
}



/*
    Metodo: int Screen::Serializa( String item )
        Imprime una cadena caracter a caracter
        un caracter por cada llamada al programa

    NOTA: Deberia retocarse en una version posterior
          y asociarle un Mensaje
*/
int Screen::Serializa( char *item )
{
  static int cont=NO;
  //  Salida
  if( !*(item+cont) )
  {
    cont = NO;
    return SI;
  }
  else
  {
    GoTo( 15 + cont , 3 );
    LCD.print( *(item+cont) );
    cont++;
    return NO;
  }
}



/*
    Metodos:    RefrescaConsigna()
                RefrescaValorEnEdicion  ()

      Metodos "comodin" para procesar la edicion de cualquier alarma
      Como la estructura almacena valores enteros, hay que pasarlos a char[]
      para imprimirlos en el LCD fisico caracter a caracter

      Para ello se emplean los mensajes
        texto[SCREEN_MSG_REFCONSGN]
        texto[SCREEN_MSG_REFVALOR]

      con el metodo ultraitoa() codificada

      Llamado por
      Screen::RefrescaEncoder()           que no se usa  Hay que aprovecharla
      MaquinaEstado::RefrescaEstado()    (en un comentario )  RefrescaEstado()
*/
int Screen:: RefrescaConsigna( int valor )
{
  ultraitoa(valor , &texto[SCREEN_MSG_REFCONSGN].cadena[0] );
  return( PrintMensaje( SCREEN_MSG_REFCONSGN ) );
}


int Screen:: RefrescaTrigger( int valor )
{
  if( valor )
    return( PrintMensaje( SCREEN_MSG_TRIGGERON ) );
  else
    return( PrintMensaje( SCREEN_MSG_TRIGGEROFF ) );
}



int Screen:: RefrescaValorEnEdicion( int valor )
{
  ultraitoa(valor , &texto[SCREEN_MSG_REFVALOR].cadena[0] );
  return( PrintMensaje( SCREEN_MSG_REFVALOR ) );
}



//  No va
int Screen:: RefrescaTriggerenEdicion( int valor )
{
  if( valor )
    return( PrintMensaje( 50 ) );
  else
    return( PrintMensaje( 51 ) );
}



/*
    Metodo: RefrescaEncoder
      Actualiza en el LCD el dato que se esta editando
      Se muestra cada XTAL_ENCODER_REFRESCO ciclos

      Llamado por
      MaquinaEstado::RefrescaEstado()

      par:  Estados Posibles, que son
          HID_ESTADO_MENU_ALARMAS para refrescar el menu de alarmas
          HID_ESTADO_ALARMA_TRIGGER Alarma especial con valor logico { SI , NO }
          resto Refresco de una consigna en edicion con el encoder
*/
int Screen::RefrescaEncoder( int par )
{
  char * cadena;
  switch( par )
  {
    //  Estamos en el Menu de seleccion de alarma
    case  HID_ESTADO_MENU_ALARMAS:
        cadena = &menuItems[lcd2004.miBotonera.miEncoder.EncoderGetCursor()][0];
        return ( Serializa( cadena ) );
        //  Edicion de la consigna Trigger
        //    No muestra un valor numerico, sino una cadena { "SI" , "NO" }
    case  HID_ESTADO_ALARMA_TRIGGER:
        Serial.print( par );
        if( par )
          return( PrintMensaje( SCREEN_MSG_REFTRIGGERON ) );
        else
          return( PrintMensaje( SCREEN_MSG_REFTRIGGEROFF ) );
    break;
    //  Estamos Editando una consigna
    default:
        return RefrescaValorEnEdicion( lcd2004.miBotonera.miEncoder.EncoderGetActual() );
    break;
  }
  return SI;
}


/*
  Metodo: ConsignasConvierte()

      ItoaLotes convierte 6 variables de tipo int en 6 cadenas de char[4]
      La funcion ItoaLotes() se llama 6 veces antes de que finalice
      Devuelve 0 ( si no está completada ) o ( 1 )
      Se llama a ultraitoa()  - 4 usg por conversion -
      ConsignasMain() muestra las consignas en las filas 2 y 3
*/

int Screen::ConsignasConvierte( void )
{
   //  ItoaLotes()  termina con 1 , no con 0
    return  ItoaLotes();
}



/*
    Metodo: ItoaLotes()

      Devuelve: 0 si no ha acabado el proceso
            1 cdo ha llegado al ultimo paso
*/
int Screen::ItoaLotes( void )
{
  static char numerociclo= CICLO_INICIAL;      //  No hace falta un gastar dos bytes si con uno me sobra
  static int salida;

  switch ( numerociclo )
  {
    case CICLO_INICIAL:
      salida = 0;
      ultraitoa( lcd2004.misConsignas.frecRPMconsigna ,  &texto[SCREEN_MSG_CONSGN_RPM].cadena[0] );
    break;
    case CICLO_02:
      ultraitoa( lcd2004.misConsignas.frecRPMactual , &texto[SCREEN_MSG_ACTUAL_RPM].cadena[0] );   //lcd2004.pantalla.bufferfrecRPMactual );
    break;
    case CICLO_03:
      ultraitoa( lcd2004.misConsignas.presionPICOconsigna ,  &texto[SCREEN_MSG_CONSGN_PICO].cadena[0] );   //lcd2004.pantalla.bufferpresionPICOconsigna );
    break;
    case CICLO_04:
      ultraitoa( lcd2004.misConsignas.presionPICOactual ,  &texto[SCREEN_MSG_ACTUAL_PICO].cadena[0] );   //lcd2004.pantalla.bufferpresionPICOactual );
    break;
    case CICLO_05:
      ultraitoa( lcd2004.misConsignas.presionPEEPconsigna ,  &texto[SCREEN_MSG_CONSGN_PEEP].cadena[0] );   //lcd2004.pantalla.bufferpresionPEEPconsigna );
    break;
    case CICLO_06:
      ultraitoa( lcd2004.misConsignas.presionPEEPactual ,  &texto[SCREEN_MSG_ACTUAL_PEEP].cadena[0] );   //lcd2004.pantalla.bufferpresionPEEPactual );
    break;
    case CICLO_07:
      ultraitoa( lcd2004.misConsignas.volumenactual ,  &texto[SCREEN_MSG_ACTUAL_VOL].cadena[0] );   //lcd2004.pantalla.bufferpresionPEEPactual );
      salida=1;
      numerociclo = CICLO_FINAL;                              //  Y lo ponemos a cero al finalizar
    break;
    default:
        salida = 1;
        numerociclo = CICLO_FINAL;                           //  Y lo ponemos a cero al finalizar
    break;
  }
  //    Incrementa el ciclo. Ojo es obligatorio incluir en algun caso CICLO_FINAL, lo que originaria errores
  numerociclo++;
  return salida;
}





/*
  Metodo: ultraitoa

      Convierte un numero entero en una cadena de caracteres

        Condiciones de entrada
          El numero debe ser menor que 999
          La cadena debe estar reservada previamente
*/
void Screen::ultraitoa( int valor , char *salida )
{
    int contcentenas=-1;
    if( valor > 999 )
      return;
    if( valor <=0 )
      return;

    while( valor>=0 )
    {
      valor-=100;
      contcentenas++;
    }
      //    valor es negativo , le sumamos 100 y estamos en el rango { 0 , 99 }
    valor+=100;
    *salida = '0'+contcentenas;
    salida++;

    contcentenas=-1;
    while( valor>=0 )
    {
      valor-=10;
      contcentenas++;
    }
    //    valor es negativo , le sumamos 100 y estamos en el rango { 0 , 99 }
    valor+=10;
    *salida = '0'+contcentenas;
    salida++;

    *salida = '0' + valor;
    salida++;
    *salida=0;
}




/*
    Metodo:   void Screen::ExternalInput( void )

    NOTA: Es un API a otros modulos del sistema
*/
void Screen::ExternalInput( void )
{
}



/*
    Metodo:   void Screen::Setup()

*/
void Screen::Setup()
{
  static int b1,b2,b3,b4;
  //    Pantalla de Bienvenida
  LCD.begin( 20 , 4 );      //    Segun la version de LiquidCrystal empleada
                            //    Sustituir por LCD.begin(); si da error
  LCD.clear();

  while( !b4 )
  if( b1 == NO )
  {
    b1 = PrintMensaje( SCREEN_MSG_33 );

  }
  else
  {
    if( b2 == NO )
      b2 = PrintMensaje( SCREEN_MSG_34 );
    else
    {
      if( b3 == NO )
        b3 = PrintMensaje( SCREEN_MSG_35 );
      else
      {
        if( b4 == NO )
          b4 = PrintMensaje( SCREEN_MSG_36 );
        else
        {
          b1 = b2 = b3 = b4 = NO;
        }
      }
    }
  }
}



int Screen::PantallaBienvenida()
{
  static int b1,b2,b3,b4;

  //    Pantalla de Bienvenida
  if( b1 == NO )
  {
    b1 = PrintMensaje( SCREEN_MSG_37 );
  }
  else
  {
    if( b2 == NO )
    {
      b2 = PrintMensaje( SCREEN_MSG_38 );
    }
    else
    {
      if( b3 == NO )
      {
        b3 = PrintMensaje( SCREEN_MSG_39 );
      }
      else
      {
        if( b4 == NO )
          b4 = PrintMensaje( SCREEN_MSG_40 );

      }
    }
  }
  return b4;
}



//  <---------------------------------------------
//    Funciones que enlazan clases Screen y MaquinaEstado
//

/*
    Metodo: SetNumAlarmas
      Imprime el numero de alarmas entre parentesis
      Se imprime Una vez por ciclo respiratorio
      Se llama desde MaquinaEstado::RefrescaEstado() antes de Loop()

    Devuelve: 1 cdo fin de procesamiento
              0 en caso contrario
*/
unsigned char Screen::SetNumAlarmas( unsigned char mensaje )
{
    switch( mensaje )
    {
      case MSG_SINCRONISMO_ON:
          return MSG_ON;
      case MSG_ON:
        if( PrintMensaje( SCREEN_SEP_00 ) )
          return MSG_01;
        else
          return MSG_ON;
     case MSG_01:
        if( PrintMensaje( SCREEN_NUMALARMAS ) )
          return MSG_02;
        else
          return MSG_01;
     case MSG_02:
        if( PrintMensaje( SCREEN_SEP_01 ) )
          return MSG_03;
        else
        return MSG_02;
     break;
     case MSG_03:
        if( PrintMensaje( SCREEN_SEP_04 ) )
          return MSG_04;
        else
        return MSG_03;
     break;
     case MSG_04:
        if( PrintMensaje( SCREEN_SEP_05 ) )
          return MSG_FINPROCESO_ON;
        else
          return MSG_04;
     break;
     default:
        return MSG_ERROR;
      break;
    }
    return MSG_ERROR;
}



/*
  Metodo: SetBarraActuales()

    Imprime los valores actuales en la fila 3 del LCD
    Una vez por ciclo Respiratorio
    Un caracter por cada ciclo

    Devuelve: 1 cdo fin de procesamiento
              0 en caso contrario

    NOTA: Asociado a
          void MaquinaEstado::RefrescaBarraActuales( void )

*/
unsigned char Screen::SetBarraActuales( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
    return MSG_ON;
    case MSG_ON:
        if( PrintMensaje( SCREEN_MSG_ACTUAL_RPM ) )
          return MSG_01;
        else
          return  MSG_ON;
    case MSG_01:
          if( PrintMensaje( SCREEN_SEP_18 ) )
            return MSG_02;
          else
            return MSG_01;
    case MSG_02:
        if( PrintMensaje( SCREEN_MSG_ACTUAL_PICO ) )
          return MSG_03;
        else
          return MSG_02;
    case MSG_03:
        if( PrintMensaje( SCREEN_SEP_19 ) )
          return MSG_04;
        else
           return MSG_03;
    case MSG_04:
        if( PrintMensaje( SCREEN_MSG_ACTUAL_PEEP ) )
          return MSG_05;
        else
          return MSG_04;
    case MSG_05:
          if( PrintMensaje( SCREEN_SEP_20 ) )
            return MSG_06;
          else
            return MSG_05;
    case MSG_06:
        if( PrintMensaje( SCREEN_MSG_ACTUAL_VOL ) )
          return MSG_07;
        else
          return MSG_06;
    case MSG_07:
        if( PrintMensaje( SCREEN_SEP_21 ) )
          return MSG_FINPROCESO_ON;
        else
          return MSG_07;
  }
  //  Aqui no se llega nunca
  return MSG_ERROR;
}



/*
  Metodo: SetBarraConsignas()

    Imprime las consignas en la fila 2 del LCD
    Una vez por ciclo Respiratorio
    Un caracter por cada ciclo

    Devuelve: 1 cdo fin de procesamiento
              0 en caso contrario

    NOTA: Asociado a
          void MaquinaEstado::RefrescaBarraConsignas( void )
*/
unsigned char Screen::SetBarraConsignas( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
    return MSG_ON;
    case MSG_ON:
        if( PrintMensaje( SCREEN_MSG_CONSGN_RPM ) )
          return MSG_01;
        else
          return  MSG_ON;
    case MSG_01:
          if( PrintMensaje( SCREEN_SEP_12 ) )
            return MSG_02;
          else
            return MSG_01;
    case MSG_02:
        if( PrintMensaje( SCREEN_MSG_CONSGN_PICO ) )
          return MSG_03;
        else
          return MSG_02;
    case MSG_03:
        if( PrintMensaje( SCREEN_SEP_13 ) )
          return MSG_04;
        else
           return MSG_03;
    case MSG_04:
        if( PrintMensaje( SCREEN_MSG_CONSGN_PEEP ) )
          return MSG_05;
        else
          return MSG_04;
    case MSG_05:
          if( PrintMensaje( SCREEN_SEP_14 ) )
            return MSG_06;
          else
            return MSG_05;
    case MSG_06:
      if( lcd2004.miMaquinaEstado.miRegistroEstado.AlarmaTRIG )
      {
        if( PrintMensaje( SCREEN_CONSGN_TRGGON ) )
            return MSG_07;
        else
           return MSG_06;
      }
      else
      {
        if( PrintMensaje(  SCREEN_CONSGN_TRGGOF ) )
            return MSG_07;
        else
           return MSG_06;
      }
    case MSG_07:
        if( PrintMensaje( SCREEN_SEP_17 ) )
          return MSG_FINPROCESO_ON;
        else
          return MSG_07;
  }
  //  Aqui no se llega nunca
  return MSG_ERROR;
}



/*
  Metodo: BarraSuperior()
  Similar a BarraInferior()

  Devuelve: 1 cdo fin de procesamiento
            0 en caso contrario

*/
unsigned char Screen::SetBarraSuperior( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
      return MSG_ON;
    break;
    case MSG_ON:
      if( PrintMensaje( SCREEN_BARSUP_ON ) )
        return MSG_FINPROCESO_ON;
      else
        return MSG_ON;

    case MSG_SINCRONISMO_OFF:
        return MSG_OFF;
    case MSG_OFF:
      if( PrintMensaje( SCREEN_BARSUP_OF ) )
        return MSG_FINPROCESO_OFF;
      else
        return MSG_OFF;
    default:
      return MSG_ERROR;
    break;
  }
  return MSG_ERROR;
}



/*
  Metodo: SetBarraInferior()

    Imprime la Barra Inferior de Alarmas

      Sigue un protocolo, de comunicacion con la clase MaquinaEstado

  Devuelve: 1 cdo fin de procesamiento
            0 en caso contrario

*/
unsigned char Screen::SetBarraInferior( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
        return MSG_ON;
    case MSG_ON:
        if( PrintMensaje( SCREEN_BARINF_RPM ) )
          return MSG_FINPROCESO_ON;
        else
          return MSG_ON;
    default:
      return MSG_ERROR;
    break;
  }
  return MSG_ERROR;
}





/*
  Metodo: SetRotuloAlarma()

    Gestiona el Rotulo de Alarma

      Simula el parpadeo escribiendo y borrando el Rotulo ( VOL / Alarm )
      Sigue un protocolo, de comunicacion con la clase MaquinaEstado

  Devuelve: 1 cdo fin de procesamiento
            0 en caso contrario
*/
unsigned char Screen::SetRotuloAlarma( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
        return MSG_ON;
    case MSG_ON:
      if( !modoEdicion )
      {
        if( PrintMensaje( SCREEN_BARINF_VOLON ) )
         return MSG_FINPROCESO_ON;
        else
          return MSG_ON;
      }
    break;
    case MSG_SINCRONISMO_OFF:
    return MSG_OFF;
    case MSG_OFF:
      if( !modoEdicion )
      {
        if( PrintMensaje( SCREEN_BARINF_VOLOF ) )
          return MSG_FINPROCESO_OFF;
        else
          return MSG_OFF;
      }
    break;
    default:
      return MSG_ERROR;
    break;
  }
  return MSG_ERROR;
}



/*
    Metodo: PantallaEdicion()

        Imprime la fila segunda del LCD

        Devuelve:
              1 al finalizar
              0 si necesita mas llamadas

*/
unsigned char Screen::SetPantallaEdicion( unsigned char mensaje )
{
  switch( mensaje )
  {
    case MSG_SINCRONISMO_ON:
    return MSG_ON;

    case MSG_ON:
      switch( estadoEdicion )
      {
        case HID_ESTADO_SWITCHRPMPULSADO:
          if( PrintMensaje( SCREEN_MSG_22 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_SWITCHPICOPULSADO:
          if( PrintMensaje( SCREEN_MSG_23 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_SWITCHPEEPPULSADO:
          if( PrintMensaje( SCREEN_MSG_24 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_RPM_MINIMO:
          if( PrintMensaje( SCREEN_MSG_26 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_RPM_MAXIMO:
          if( PrintMensaje( SCREEN_MSG_27 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_PICO_MINIMO:
          if( PrintMensaje( SCREEN_MSG_28 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_PICO_MAXIMO:
          if( PrintMensaje( SCREEN_MSG_29 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_PEEP_MINIMO:
          if( PrintMensaje( SCREEN_MSG_30 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_PEEP_MAXIMO:
          if( PrintMensaje( SCREEN_MSG_31 ) )
            return MSG_01;
          else
            return MSG_ON;
        case HID_ESTADO_ALARMA_TRIGGER:
          if( PrintMensaje( SCREEN_MSG_32 ) )
            return MSG_01;
          else
            return MSG_ON;
        default:
            Serial.println("Error en Modulo: PantallaEdicion");
        break;
      }
    case MSG_01:
      switch( estadoEdicion )
      {
        case HID_ESTADO_SWITCHRPMPULSADO:
          if( RefrescaConsigna( lcd2004.misConsignas.frecRPMconsigna ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_SWITCHPICOPULSADO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPICOconsigna ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_SWITCHPEEPPULSADO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPEEPconsigna ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_RPM_MINIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.frecRPMminimo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_RPM_MAXIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.frecRPMmaximo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_PICO_MINIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPICOminimo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_PICO_MAXIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPICOmaximo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_PEEP_MINIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPEEPminimo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_PEEP_MAXIMO:
          if( RefrescaConsigna( lcd2004.misConsignas.presionPEEPmaximo ) )
            return MSG_02;
          else
            return MSG_01;
        case HID_ESTADO_ALARMA_TRIGGER:
          // Esta debe mostrar "SI" "NO" a partir de { 1 , 0 }
          if( RefrescaTrigger( lcd2004.misConsignas.valorTRIGGER ) )
            return MSG_02;
          else
            return MSG_01;
        default:
            Serial.println("Error en Modulo: PantallaEdicion");
        break;
      }
    case MSG_02:
      if( PrintMensaje( SCREEN_MSG_25 ) )
        return MSG_03;
      else
        return MSG_02;
    case MSG_03:
      switch( estadoEdicion )
      {
        case HID_ESTADO_SWITCHRPMPULSADO:
          if( RefrescaValorEnEdicion( lcd2004.misConsignas.frecRPMconsigna ) )
            return MSG_FINPROCESO_OFF;
            return MSG_03;
        case HID_ESTADO_SWITCHPICOPULSADO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPICOconsigna ) )
            return MSG_FINPROCESO_OFF;
            return MSG_03;
        case HID_ESTADO_SWITCHPEEPPULSADO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPEEPconsigna ) )
            return MSG_FINPROCESO_OFF;
            return MSG_03;
        case HID_ESTADO_ALARMA_RPM_MINIMO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.frecRPMminimo ) )
            return MSG_FINPROCESO_OFF;
          else
            return MSG_03;
        case HID_ESTADO_ALARMA_RPM_MAXIMO:
            if(  RefrescaValorEnEdicion( lcd2004.misConsignas.frecRPMmaximo ) )
              return MSG_FINPROCESO_OFF;
            else
              return MSG_03;
        case HID_ESTADO_ALARMA_PICO_MINIMO:
            if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPICOminimo ) )
              return MSG_FINPROCESO_OFF;
            else
              return MSG_03;
        case HID_ESTADO_ALARMA_PICO_MAXIMO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPICOmaximo ) )
            return MSG_FINPROCESO_OFF;
          else
            return MSG_03;
        case HID_ESTADO_ALARMA_PEEP_MINIMO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPEEPminimo ) )
            return MSG_FINPROCESO_OFF;
         else
            return MSG_03;
        case HID_ESTADO_ALARMA_PEEP_MAXIMO:
          if(  RefrescaValorEnEdicion( lcd2004.misConsignas.presionPEEPmaximo ) )
            return MSG_FINPROCESO_OFF;
          else
            return MSG_03;
        case HID_ESTADO_ALARMA_TRIGGER:
          // Esta debe mostrar "SI" "NO" a partir de { 1 , 0 }
          if( RefrescaTriggerenEdicion( lcd2004.misConsignas.valorTRIGGER ) )
            return MSG_FINPROCESO_OFF;
          else
            return MSG_03;
        default:
            Serial.println("Error en Modulo: PantallaEdicion");
        break;
      }
  }
  return SI;
}

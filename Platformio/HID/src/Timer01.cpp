/*
     Modulo: Timer01

      Funciones:
        void SetupTimer(void)
        void SetupTimer(void)
        void RutinaISR( void )
        void TestLed( void )

          Ejemplo extraido de la libreria TimerOne

  NOTA:   Hay que Implantarlo
          Realmente no es necesario, ya que el programa al devolver
          el control a loop() cada 250 usg ( sin la rutina de bajo nivel
          de la libreria LCD ) se puede simular
          con un simple delay(1) en loop()

 */
#include "HID.h"
extern Hid lcd2004;

volatile unsigned long int  contadorticksTestLed;
volatile bool               flagticksTestLed;



/*
    Funcion: SetupTimer()


*/
void SetupTimer(void)
{
  Timer1.initialize( HID_XTAL );
  Timer1.attachInterrupt( RutinaISR );
}



/*
    Funcion: RutinaISR( void )
      Incrementa los contadores internos del sistema y la maquina hid
 */

void RutinaISR( void )
{
  contadorticksTestLed++;
  flagticksTestLed++;
}



/*
    1) TestLED

          Sirve para hacer que un LED parpadee con nuestra base de tiempos
          Verifica el correcto funcionamiento de TimerOne



    Nota: Implantarlo
 */
void TestLed( void )
{
}



/*
    Funcion: loopTestLed( void )

    NOTA: Implantarlo
 */
void loopTestLed( void )
{
    noInterrupts();
    lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.flagticks     = flagticksTestLed;
    lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.contadorticks = contadorticksTestLed;
    interrupts();
}

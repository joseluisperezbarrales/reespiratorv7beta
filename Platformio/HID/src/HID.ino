/*

    Hid para Reespirator  v 1.0

    Release:          1.0
    Release Number:   0.65


    Fecha inicio: 04/04/2020
    Descripcion:

      Modulo HID ( Human Interface Device ) para maquina Reespirator
      Version para LCD (4 x 20 ) serie

      Caracteristicas principales:

        1)  Cada ciclo de loop() se ejecuta en menos de

          250 usg con la libreria LiquidCristal
          <50 usg usando librerias de acceso directo al LCD
          ( como por ejemplo LCDHal  de Alberto Ferrer )

          El tiempo en cada ciclo de loop es una especificacion de partida

        2)  En cada ciclo como maximo se imprime un caracter por el LCD. No se
          implanta ningun tipo de cola, etc.

        3)  Responde a los botones de consigna principal RPM PICO PEEP

        4)  Integra un encoder, que permite seleccionar consignas secundarias

        5)  Permite editar las consignas principales y secundarias


      El fichero README.txt contiene las notas de desarollo ultimas

   1) To Do:
        La pantalla de Configuracion incial no esta implantada
        Incluir soporte para buzzer
        Gestionar Alarmas Rotatorias
        Encapsular y sanear las clases
            ( Que es privado y que publico )
        Implantar los metodos Set() y Get()
          (
            para eliminar llamadas como lcd2004.pantalla.modoEdicion = 1 ;
            sustituirlas por algo como  lcd2004.pantalla.SetModoEdicion( 1 );
          )

   Autor: Jose Luis Perez Barrales
          hispalisrobiotics@gmail.com

          en colaboracion con el grupo
          CVMakers_Reesp_ARD de Telegram

          Especial mencion para Eulogio ·Lalio", colaborador

   Andalucia
   Spain
 */
#include  "HID.h"


/*
      Variables globales
 */

Hid lcd2004;                        //  Ojeto de la clase global necesario
class Consigna misDatosExternos;    //  Estructura  comun interfaz con resto modulos

void setup()
{
  lcd2004.Setup();
  delay(4000);                      //  Esperamos tras imprimir una pantalla de Inicio
                                    //  Unico delay de la aplicacion
  SetConsignasExternas();           //  Simulamos datos del exterior una sola vez
  lcd2004.HidSet( HID_ESTADO_RESET );
}


void loop()
{
  lcd2004.HidMain();
}



/*
    Este bloque puede sustituir a loop()
    para probar la velocidad del programa en cada ciclo

    El funcionamiento se reta.....aaaaaarrrrddddddaaaaaa
*/
/*
{
    long anterior , actual , diferencia;
    static long alfa  = -HORIZONTE;
    static long beta  = +HORIZONTE;

    anterior = micros();
    lcd2004.HidMain();
    actual = micros();
    diferencia = actual - anterior;

    //  Beta equivale a menor

    if( diferencia < beta)
    {
      beta = diferencia;
      Serial.print("Loop menor ... ");Serial.print( beta );
      Serial.println("  usg ");
      Tracer(SNAPSHOT);
      Tracer( TIME );
    }
    else if( diferencia > alfa)
    {
      alfa = diferencia;
      TAB;TAB;TAB;TAB;TAB;TAB;    Serial.print("Loop Mayor ... ");Serial.print( diferencia);
      Serial.println("  usg ");
      TAB;TAB;TAB;TAB;TAB;TAB;    Tracer(SNAPSHOT);
      Tracer( TIME );
    }
    else
    {
      Serial.print("Tiempo ... ");Serial.print( diferencia);
      Serial.println("  usg ");
    }
}
*/

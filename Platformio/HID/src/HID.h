/*
    HID.h

    Fichero de cabecera
    Declara las clases

        class MaquinaEstado
        class Screen
        class RotaryEncoder
        Respirator
        Pantalla
        RelojExterno

    La clase principal ( Hid ) esta formada por
      un objeto   MaquinaEstado
      un objeto   Botonera
      un objeto   Encoder
      un objeto   Pantalla
      un objeto MechVentilator para modelar el nucleo de la clase Hid

      ( El objeto MechVentilator se deja en blanco y se añadirá posteriormente )
      La presente aplicacion desarolla el resto de los modulos
 */

#include  <Arduino.h>
#include  <Wire.h>
#include  <LiquidCrystal.h>
#include  <TimerOne.h>
/*
      Macros de preprocesador
 */
typedef unsigned char ESTADO_MAQUINA;


//  Lcd I2C o Paralelo
#define I2C
#define PARALELO



//      Esto se lo dedico a los programadores .... "Se es o no se es, pero no lo intentes"
//      Me lo agradeceran la proxima vez que depuren
#define FOREVER                         for(;;)
#define FIN_FOREVER                     for(;;);

#define SI                                    1
#define NO                                  !SI

//        Pinout de la placa
#define ENCODER_DT                            43    //   Verde
#define ENCODER_CLK                           45    //   Azul
#define SWITCH_ALARMAS                        47    //   Naranja
#define SWITCH_RPM                            49    //   Verde
#define SWITCH_PICO                           51    //   Violeta
#define SWITCH_PEEP                           53    //   Azul
#define BUZZER                                11    //   Buzzer no implantado todavia
#define MAXROWS                                4
#define MAXCOLS                               20
#define INTERRUPCIONI2C                     0x27    //    Depende del hardware

//  Puerto Serie
#define BAUDIOS                           115200

//  Bases de tiempo para los osciladores internos

#define XTAL_EXTERNO1                     200000    //    10 sg en mi Pentium antiguo

#define XTAL_BARRASUP                      35000
#define TIME_BLINKBARRASUP                 30000
#define XTAL_BARRAINF                      50000
#define TIME_BLINKBARRAINF                 25000    //  Y termina aqui ( frecAlarmas )

#define XTAL_TECLADO                       10000    //    Muestreo cada 5 msg
#define TECLADO_TRENPULSOS                   500

//#define XTAL_ENCODER                           5    //1500
//#define ENCODER_TRENPULSOS                     1    //500
#define XTAL_ENCODER_REFRESCO               1000
#define ENCODERTIMEOUT                     10000

#define ENCODER_DUREZA_MAS                     3
#define ENCODER_DUREZA_MENOS                   3


//    En Timer=n3
#define HID_XTAL                             100
#define CLOCK_LED                           1500    //    150  msg

//    Permite hacer un echo de la salida del programa por el monitor Serie
//    Para mostrarla, solo hay que quitar el comentario a la linea inferior
//    Para anularla, solo hay que comentar la linea inferior


//#define SALIDAPANTALLA  1

//  Usadas en Itoa y Plantilla.cpp
#define CICLO_FINAL                            0
#define CICLO_INICIAL                          1
#define CICLO_02                               2
#define CICLO_03                               3
#define CICLO_04                               4
#define CICLO_05                               5
#define CICLO_06                               6
#define CICLO_07                               7
#define CICLO_08                               8
#define CICLO_09                               9
#define CICLO_10                              10
#define CICLO_11                              11
#define CICLO_12                              12
#define CICLO_13                              13
#define CICLO_14                              14
#define CICLO_15                              15
#define CICLO_16                              16

//    Empleadas en Tracer()     ( Depurador sistema )
#define SNAPSHOT                              15
#define STATUS                                16
#define ESTADOBOTONERA                        17
#define STEP                                  18
#define BUFFER                                19
#define TIME                                  20
#define INFO                                  21
#define DATA                                  22

//    Estas macros se pueden incluir en el programa para rastrear el programa
//    ABRELOOP y CIERRALOOP podrian originar errores
//    si no se incluyen en el sitio correcto  (por ejemplo en medio de un bucle )
//
#define TRACER_CONTADORPROGRAMA           Tracer( SNAPSHOT );
#define TRACER_RETARDO                    Tracer( TIME );
#define TRACER_HIDESTADO                  Tracer( STATUS );
#define TRACER_INFO                       Tracer( INFO );
#define TRACER_ABRELOOP                   for(;;){
#define TRACER_CIERRAlOOP                 }

//    < ----------------    Fin defincion macros

/*

    Clase PAQUETE_CONSIGNA

    Simula los datos  externos
    entran aproximadamente cada 6 / 7 segundos
    Acceden por con la frecuencia definida por XTAL1 a traves de flagext1 = true

    Se intentará conectarle 2 BMP280

    Estos datos se cargan en una estructura simila en la clase HID

    Esto supone el inicio del ciclo respiratorio y es una señal de sincronia para hid

    Editables :

    int frecRPMminimo;
    int frecRPMmaximo;
    int presionPEEPminimo;
    int presionPEEPmaximo;
    int presionPICOminimo;
    int presionPICOmaximo;
    int RECRUITminimo = 0  ( Es cuenta descendente de tiempo )
    int RECRUITmaximo;

    int presionRPMconsigna;
    int presionPICOconsigna;
    int presionPEEPconsigna;
    int volumenconsigna;
    int valorTRIGGER;
*/

class Consigna
{
  public:
  int volumenminimo;
  int volumenmaximo;
  int volumenconsigna;
  int volumenactual;

  int presionPICOminimo;
  int presionPICOmaximo;
  int presionPICOconsigna;
  int presionPICOactual;

  int presionPEEPminimo;
  int presionPEEPmaximo;
  int presionPEEPconsigna;
  int presionPEEPactual;

  int frecRPMminimo;
  int frecRPMmaximo;
  int frecRPMconsigna;
  int frecRPMactual;

  int valorTRIGGER;

  int valorRECRUIT;
  int valorRECRUITmaximo;   // El minimo no tiene sentido, es cero
  int bateriaLOW;

  //    Flags de ALARMA
  int flagAlarmaRPM;
  int flagAlarmaPICO;
  int flagAlarmaPEEP;
  int flagAlarmaRPMBajo;
  int flagAlarmaRPMAlto;
  int flagAlarmaPICOBajo;
  int flagAlarmaPICOAlto;
  int flagAlarmaPEEPBajo;
  int flagAlarmaPEEPAlto;

};
typedef class Consigna PAQUETE_CONSIGNAS;





/*

    Clase Encoder

    Modelado simple de un encoder KEYS
 #define */
 class RotaryEncoder
{
  public:

    int switchanterior;
    int switchactual;
    int switchconsigna;                  //  estado de la consigna a editar

    int valorminimo;
    int valormaximo;
    int valorconsigna;                  //  El original al pulsar algun switch el operario
    int valoractual;                    //  El que esta cambiando el operario right now !
    int incremento;                    //  El que esta cambiando el operario right now !

    int pasosmas;                       //  Permite hacer mas rapido o lento el encoder
    int pasosmenos;                     //  Permite hacer mas rapido o lento el encoder

    int  cursor;


    unsigned long frecRefresco;         //  Refresco de salida de Encoder
    unsigned long clocktimeout;              //  Contador del Encoder. AL rebasarlo se devuelve timeout
    unsigned long conttimeout;                    //  Multiplicador de timeouts
    unsigned long prescaler;                      //  Contador interno


    //    Metodos
    void Reset( int );
    int LeeEncoder( void );
    int LeeSwitch( void );

    int EncoderGetCursor( void )         { return cursor; }
    int EncoderGetActual( void )         { return valoractual; }
    int EncoderGetConsigna( void )       { return valorconsigna; }
    int EncoderGetSwitchAnterior( void ) { return switchanterior; }
    int EncoderGetSwitchActual( void )   { return switchactual; }

    //  State of the art
    int ModificaValor( int );
    int Menu( void );
};





/*
    Clase Screen

    Constituye el modelo de pantalla de la maquina Respirator
    Display de 4 lineas de caracteres  ( 20 x 4 )

    A nivel conceptual esta compuesta por 2 capas

    1)    Nivel superior: 4 variables de tipo char *
    2)   Nivel inferior: clase LyquidCrystal2I2C

          A este nivel hemos incluido la conexion con el hardware
          De este modo una pantalla puede admitir diferente hardware
          Nuestro modelo se basa en el display LCD  4x20 I2C
          Hemos probado programas ejemplo de base, que emplean otra interrupcion
          sin embargo con este hardware el LCD ha funcionado perfectamente

          Al principio de este fichero se encuentra una macro que permite probar
          otras direcciones  ...suerte si has llegado a leer esto :-D

     #define INTERRUPCIONI2C             0x27

     3HID_ESTADOREFRESCAALRMAS) Prueba con LCD serie en vez de I2C

        En teoria cambiando solo el constructor

        LCD( INTERRUPCIONI2C , 20 , 4 )

        por algo como

        LiquidCrystal LCD(12, 11, 10, 5, 4, 3, 2);

        Esto se incluira en futuras versiones

      4)  Mejoras propuestas
        Programar el chip directamente es lo mas rapido
        Problema en contra: Portabilidad

      5)  Feedback  Diversas configuraciones hardware deberian considerarse,
        dado el numero de fabricantes de hardware diferentes

        Es por esto que se sugiere crear un banco de hardware "fiable"
 */
class Screen
{
  friend class MaquinaEstado;
  friend class Encoder;

  private:
    char *linea1;
    char *linea2;
    char *linea3;
    char *linea4;

  public:
    int  modoEdicion;             //    { SI , NO }

    unsigned char estadoEdicion;           //    necesaria en RefrescaPantallaEdicion()

    LiquidCrystal LCD;
    //    Constructor   -->
    //    Llama al constructor de LiquidCrystalI2C ( Ojo, tiene 3 argumentos ! )
    //    Cambiar la primera direccion si el chip no funciona
    //
    //    Brillante, elegante y simple
          Screen():LCD( 48 , 46 , 44, 42 , 40 , 38 , 36 , 34 , 32 , 30 , 28 ) {}
    //    Screen():LCD( INTERRUPCIONI2C , 20 , 4 ){}
    //    Como opcion alternativa
    //    la linea anterior debe sustituirse por algo similar a esto
    //    Screen():LiquidCrystal lcd( 48 , 46 , 44, 42 , 40 , 38 , 36 , 34 , 32 , 30 , 28 );  {}
    //    cambiando los pines

    //    Metodos
    void Setup( void );
    void SetEstadoEdicion( unsigned char x ) { estadoEdicion = x ; }
    int PantallaBienvenida();
    void ExternalInput( void );

    unsigned char SetNumAlarmas( unsigned char mensaje );
    unsigned char SetBarraSuperior( unsigned char mensaje );
    unsigned char SetBarraInferior( unsigned char mensaje );
    unsigned char SetRotuloAlarma( unsigned char mensaje );
    unsigned char SetBarraConsignas( unsigned char mensaje );
    unsigned char SetBarraActuales( unsigned char mensaje );
    unsigned char SetPantallaEdicion( unsigned char mensaje );

    int ConsignasConvierte( void );
    int ItoaLotes( void );
    void ultraitoa( int valor , char *salida );


    //  Empleada por Encoder
    int RefrescaEncoder( int );
    int RefrescaConsigna(  int valor );
    int RefrescaTrigger( int valor );
    int RefrescaValorEnEdicion( int valor );
    int RefrescaTriggerenEdicion( int valor );
    int Serializa(  char *cadena );

    //  Enlace con bajo Nivel
    void GoTo( int col , int fil );
    int PrintVariable( int numero );
    int PrintMensaje( int numero);
};




/*

    Clase Botonera

 */

//  Configuracion del buffer de la Botonera

#define BOTONERA_BUFFER       128                           //    Numero de estados ( 1 estado cada msg ) que se pueden almacenar
#define BOTONERA_FLANCO        30                           //      Al subirlo, el switch tarda mas en activar ON
#define BOTONERA_UMBRAL_ON     20

class Botonera
{
  public:
    ESTADO_MAQUINA bufferbotonera[BOTONERA_BUFFER+10];     //    Almacena el estado instantaneo de las entradas
    ESTADO_MAQUINA indicebuffer, bufferlleno;              //    Indice actual de muestras
    ESTADO_MAQUINA estadoactual , estadoanterior;
    RotaryEncoder  miEncoder;

    //    Metodos
    void Reset( void );
    ESTADO_MAQUINA EstadoBotonera( void );                       //     Obtiene el estado del sistema
    ESTADO_MAQUINA  GetEstado( void );
    ESTADO_MAQUINA MuestreaTeclado( void );
    ESTADO_MAQUINA ObtenEstadoPorlotes( void );

    int ModificaValor( void );
};


/*

    Clase Mechventilator

      Modelado simple de un encoder KEYS
 */

class MechVentilator
{};





//    Marca hasta 3 ticks internos para el sistem
//      Los osciladores internos permten realizar procesos cada cierto tiempo
//      Por ejemplo, la Botonera se puede muestrear cada x ciclos en vez de cada loop()
//      Asi descargamos al sistema
//
class RelojExterno
{
  public:

    volatile unsigned long int contadorticks;
    volatile int flagticks;

    unsigned long int frecsistema;                 //  Sincroniza señal de ciclo respiratorio
    unsigned long int frecTeclado;                 //  Refresco de Teclado necesario
    unsigned long int frecBarraSup;
    unsigned long int frecBarraInf;
    unsigned long int frecEdicion;

    unsigned int contpulsosTeclado;                //  Pulsos que se generan

    //    Flags de Refresco de Pantalla
    int flagTeclado;
    int flagConsignas;
    int flagBarraInf;
    int flagBarraSup;
    int flagEncoder;

    //    Metodos()
    void Late( void );
    void Reset( void );
    void Pulso( void );
};




class RegistroEstado
{
  public:
      char                   alarmasactivas;
      int                    AlarmaRPM;
      int                    AlarmaPICO;
      int                    AlarmaPEEP;
      int                    AlarmaTRIG;

      //    Gestionan los mensajes entre clases MaquinaEstado y pantalla
      unsigned char          recibidoNumAlarmas;
      unsigned char          recibidoBarraInf;
      unsigned char          recibidoBarraSup;
      unsigned char          recibidoBarraCon;
      unsigned char          recibidoBarraAct;
      unsigned char          recibidoBarraEdc;

      RelojExterno           miRelojInterno;

      //    Metodos
      void Reset( void );
};

  /*
      Estados de entrada del sistema

      1)  Descripcion

         El estado de entrada del sistema a nivel hardware puede almacenarse en un byte ( unsigned char )
         Como un byte equivale a un 8 digitos binarios , los cuatro bits menos significativos
         almacenan el estado de los cuatro switches
         La posicion de cada bit se activa o no segun la entrada pertinente este en estado ALTO o BAJO

         Por otra parte , el estado del encoder puede ser { -1 , 0 , 1 } . Estos tres estados
         se codifican en binario con 2 bits, que se situan a la izquierda del nibble de switches
         El encoder devuelve { -1 , 0 , 1 } , y su estado se codifica en dos bits

                                              Codigo

                Encoder en posicion central       00
                Encoder girado a la derecha       01
                Encoder girado a la izquierda     10

      hidestadoactual   =   Adicionalmente nos quedan los dos bits mas significativos como posible ampliacion
          A modo de ejemplo, podrian ser
              4 interruptores
              1 Encoder

      2)  Esquema de representacion del estado del sistema

          Bit          7   6   5   4   3   2   1   0

          Estado      NE  NE   E   E   R  PI  PE  AL

          siendo

          NE: No asignado
          E:  Bit asociado al campo Encoder
          R:  Bit asociado al switch RPM
          PI: Bit asociado al switch PICO
          PE: Bit asociado al switch PEEP
          AL: Bit asociado al switch Alarmas

      3)  Ejemplos de estado

       Situacion                 Estado del sistema                 Decimal

                            NE  NE   E    E    R   PI   PE   AL

       Reposo                0   0   0     0   0    0    0    0           0
       Se pulsa switch RPM   0   0    0    0   1    0    0    0           8
       Encoder derecha       0   0    0    1   0    0    0    0           8
  */

  /*
            Declaracion de clases
                class Screen
                class RotaryEncoder
                class Botonera
                class Hid

            Uso interno
                class MaquinaEstado
                class RelojInterno
                class Mech
  */


  /*
      Clase MaquinaEstado
      Hipotesis de trabajo:

        Surgen de las reuniones del grupo de trabajo "LCD y Encoder"

         A partir de aqui el este modelo es una suposicion del autor, ya que no se dispone
         de toda la informacion, puesto que aun se esta trabajando en el diseño del modelo
         Por tanto se ha optado por encapsular en esta clase cualquier decision futura acordada

      2) Modelo presentado

         El modelo de maquina aqui presentado muestrea 1 vez cada msg la botonera ,
         Para ello se crea una base de tiempo de 1 msg mediante la libreria TimerOne

      3)  Metodos:

            void MaquinaEstado::ObtenEstado( void )
            void CambiadeEstado( void )
  */



  //    Estado Interno de la clase HID
  //
  //      He puesto los que se me ocurren
  //      Son solo una sugerencia
#define HID_ESTADO_ARRANQUE                     50
#define HID_ESTADO_SINCRONISMO_EXTERNO          51
#define HID_ESTADO_ACTUALIZACONSIGNAS           52
#define HID_ESTADO_NUMALARMAS                   53
#define HID_ESTADO_PROCESACONSIGNAS             54
#define HID_ESTADO_IMPRIMEACTUALES              55
#define HID_ESTADO_IMPRIMECONSIGNAS             56
#define HID_ESTADO_IMPRIMEBARRAINF              57
#define HID_ESTADO_LOOP                         58

#define HID_ESTADO_ENCODER_REFRESCO             45
#define HID_ESTADO_ENCODER_TIMEOUT              48

#define HID_ESTADO_SWITCHRPMPULSADO              8
#define HID_ESTADO_SWITCHPICOPULSADO             4
#define HID_ESTADO_SWITCHPEEPPULSADO             2
#define HID_ESTADO_SWITCHALARMAPULSADO           1
#define HID_ESTADO_SWITCHESOFF                  00

//  Estados Edicion de alarmas
#define HID_ESTADO_ALARMA_RPM_MINIMO            61
#define HID_ESTADO_ALARMA_RPM_MAXIMO            62
#define HID_ESTADO_ALARMA_PICO_MINIMO           63
#define HID_ESTADO_ALARMA_PICO_MAXIMO           64
#define HID_ESTADO_ALARMA_PEEP_MINIMO           65
#define HID_ESTADO_ALARMA_PEEP_MAXIMO           66
#define HID_ESTADO_ALARMA_TRIGGER               67
#define HID_ESTADO_MENU_ALARMAS                 68

//  Renombrar. o devuelve el encoder fisico
#define HID_ESTADO_ENCODER_GIRODCHA_PULSADO     32
#define HID_ESTADO_ENCODER_GIROIZDA_PULSADO     16
#define HID_ESTADO_ENCODER_CENTRO_PULSADO       00




//    Estados de la clase Hid
#define HID_ESTADO_ON                          101
#define HID_ESTADO_PAUSE                       102
#define HID_ESTADO_RESET                       103
#define HID_ESTADO_STOP                        104


//    Estados de la botonera
#define BOTONERA_ENCODER_CENTRO_PULSADO        00
#define BOTONERA_ENCODER_GIRODCHA_PULSADO      32
#define BOTONERA_ENCODER_GIROIZDA_PULSADO      16

#define BOTONERA_SWITCH_RPM_PULSADO             8
#define BOTONERA_SWITCH_PICO_PULSADO            4
#define BOTONERA_SWITCH_PEEP_PULSADO            2
#define BOTONERA_SWITCH_ALARMAS_PULSADO         1
#define SWITCH_OFF                              0



//  Sincronismo entre clases MaquinaEstado  y  Pantalla
//    Documentar
#define MSG_SINCRONISMO_ON                 60
#define MSG_ESPERANDO_ON                   61
#define MSG_ON                             62
#define MSG_FINPROCESO_ON                  63
#define MSG_ERROR                          64
#define MSG_SINCRONISMO_OFF                65
#define MSG_ESPERANDO_OFF                  66
#define MSG_OFF                            67
#define MSG_FINPROCESO_OFF                 68
#define MSG_DESCONEXION                    69


#define MSG_01                             70
#define MSG_02                             71
#define MSG_03                             72
#define MSG_04                             73
#define MSG_05                             74
#define MSG_06                             75
#define MSG_07                             76
#define MSG_11                             77
#define MSG_12                             78
#define MSG_13                             79



//  <----------  Fin Sincronismo eentre clases MaquinaEstado  y  Pantalla


class MaquinaEstado
{
    public:
    int hidestadoanterior;
    int hidestadoactual;
    class RegistroEstado  miRegistroEstado;

      //    Metodos
    void Reset( void );
    int GetEstado();
    int SetEstado( unsigned char estado );
    void RefrescaEstado( void );
    void Loop();

    void RefrescaBarraSuperior( void );
    int ParpadeaRotuloAlarma( void );

    int RefrescaNumAlarmas( void );
    int RefrescaBarraInferior( void );
    int RefrescaBarraConsignas( void );
    int RefrescaBarraActuales( void );
    int RefrescaPantallaEdicion( int );
};



/*
    Clase Hid
    Release 1.0

    ToDo:
      Encapsular
*/
//  Mensajes de HID
#define HID_ON      1
#define HID_RESET   2
#define HID_PAUSE   3
#define HID_STOP    4



class Hid
{
public:
    //    Campos
    class Pantalla:     public Screen {};              //   Repasar la implicacion de public Screen{};

    //    Componentes software de la clase
    class MaquinaEstado   miMaquinaEstado;
    class Botonera        miBotonera;
    class Pantalla        pantalla;                    //    Define el interfaz  ( LCD 4 x 20 )
    class Consigna        misConsignas;
    int flagexterno;
    //    Metodos

    void HidMain();                                   //    Funcion a incluir en loop()
    void Setup();                                     //    Inicia el hardware como en setup() clasico  y  Abre puerto serie

    //  Setters y Getters
    int HidSet( int );                   //   Activa a un estado la Maquina
    int HidGet();                                //   Devuelve el estado
    void GetConsignas( void );
    int HidVersion( int );              //    No implantada
};





//    Funciones externas

//  En modulo Timer01.cpp
//  Necesaria para crear una base de tiempo ( ver ejemplo en bibliotecaTimerOne )
void SetupTimer(void);
void RutinaISR( void );                                       //    Rutina ISR  actualmente solo modifica un flag
void RutinaPrincipal( void );                                 //    Modulo actual en desarrollo
                                                              //    Ya tenemos una Botonera con buffer y una maquina de estados

int MiFuncionPorLotes( void );

//Modulo Test del Sistema ( Tests.cpp )
#define TAB Serial.print("\t");
void Tracer( int );
void Ayuda( void );
void SetConsignasExternas( void );


//    Diario


//<----------------------------------Pasar a bonito
#define XTAL 23
int AlarmaMainPorLotes( void );



//<---------------- Definitivo




//<---------------- Anotaciones state of the art


#define HORIZONTE 1000000                                    //  para variables / consignas externas

void TestProgMem();


//    Mensajes de Texto que se imprimen en el LCD
//    Se almacenan en un array de estructuras Mensaje
//

#define ARRAYTEXTOS_LEN          55

#define SCREEN_SEP_00             0             // Fila 1 LCD
#define SCREEN_SEP_01             1             // Fila 1 LCD

#define SCREEN_BARINF_RPM         2             // Fila 3 LCD
#define SCREEN_NUMALARMAS         3
#define SCREEN_SEP_04             4
#define SCREEN_SEP_05             5
#define SCREEN_BARINF_VOLON       6

#define SCREEN_MSG_REFCONSGN      7
#define SCREEN_MSG_REFVALOR       8

#define SCREEN_BARINF_VOLOF        9             // Fila 3 LCD
#define SCREEN_BARSUP_ON          10             // Fila 0 LCD
#define SCREEN_BARSUP_OF          11             // Fila 0 LCD

#define SCREEN_SEP_12             12             // Fila 1 LCD
#define SCREEN_SEP_13             13             // Fila 1 LCD
#define SCREEN_SEP_14             14             // Fila 1 LCD
#define SCREEN_CONSGN_TRGGON      15
#define SCREEN_CONSGN_TRGGOF      16
#define SCREEN_SEP_17             17             // Fila 1 LCD

#define SCREEN_SEP_18             18             // Fila 2 LCD
#define SCREEN_SEP_19             19             // Fila 2 LCD
#define SCREEN_SEP_20             20             // Fila 2 LCD
#define SCREEN_SEP_21             21             // Fila 2 LCD

//    Empleados en PantallaEdicion
#define SCREEN_MSG_22  22
#define SCREEN_MSG_23  23
#define SCREEN_MSG_24  24
#define SCREEN_MSG_25  25
#define SCREEN_MSG_26  26
#define SCREEN_MSG_27  27
#define SCREEN_MSG_28  28
#define SCREEN_MSG_29  29
#define SCREEN_MSG_30  30
#define SCREEN_MSG_31  31
#define SCREEN_MSG_32  32
#define SCREEN_MSG_33  33
#define SCREEN_MSG_34  34
#define SCREEN_MSG_35  35
#define SCREEN_MSG_36  36
#define SCREEN_MSG_37  37
#define SCREEN_MSG_38  38
#define SCREEN_MSG_39  39
#define SCREEN_MSG_40  40

#define SCREEN_MSG_ACTUAL_RPM     41    // Fila 2 LCD
#define SCREEN_MSG_ACTUAL_PICO    42    // Fila 2 LCD
#define SCREEN_MSG_ACTUAL_PEEP    43    // Fila 2 LCD
#define SCREEN_MSG_ACTUAL_VOL     44    // Fila 2 LCD
#define SCREEN_MSG_CONSGN_RPM     45    // Fila 2 LCD
#define SCREEN_MSG_CONSGN_PICO    46    // Fila 2 LCD
#define SCREEN_MSG_CONSGN_PEEP    47    // Fila 2 LCD

#define SCREEN_MSG_TRIGGERON      48
#define SCREEN_MSG_TRIGGEROFF     49
#define SCREEN_MSG_REFTRIGGERON   50
#define SCREEN_MSG_REFTRIGGEROFF  51
class Mensaje
{
public:
  int fil;
  int col;
  String cadena;
};

#define MENU_ITEMS   8

#define HID_ESTADO_RESETLOOP   500



void Mofeta( void );

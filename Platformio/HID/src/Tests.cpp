/*
    Modulo Test

    Test del Sistema
    Funciones que solo se emplearon para depurar

        void Tracer( int );
        void Ayuda( void );
        void MideProcesoenMicros( void );
        void SetConsignasExternas( void );
*/

#include  "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;



/*
    Funcion: Tracer( int  )

      NOTAS: Debugger del sistema

        Un simple debugger

      Si incluimos en cualquier linea de codigo las siguientes macros

      Tracer( SNAPSHOT );
      Tracer( TIME );
      Tracer( STATUS );

      Simulamos un debugger con salida a Serial
      Se puede emmplier para ver el estado de la botonera, y otros parametros
 */
void Tracer(int tipo)
{
  switch (tipo)
  {
    case STATUS :
        Serial.print("Estado anterior ");Serial.println(lcd2004.miMaquinaEstado.hidestadoanterior);
        Serial.print("Estado actual    ");Serial.println(lcd2004.miMaquinaEstado.hidestadoactual);
        Serial.print("CodigoEstado ant ");Serial.println(lcd2004.miBotonera.miEncoder.switchanterior);
        Serial.print("CodigoEstado act ");Serial.println(lcd2004.miBotonera.miEncoder.switchactual);
        Serial.print("codigo Consigna  ");Serial.println(lcd2004.miBotonera.miEncoder.switchconsigna);
        Tracer( STEP );
    break;

    case TIME:
        delay(1000);                       // wait for a second
    break;

    //  Imprime una instantanea del contador del sistema
    //    Se puede añadir lineas con la variable que se desee
    case SNAPSHOT:
      Serial.print("Lcd2004 tick Ciclo ");
      Serial.println( lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.frecsistema );
    break;

    //    Detiene el programa
    case STEP:
       Serial.println(" Pulse  < E N T E R >");
       while( !Serial.available() )
         ;
       while( Serial.available() )
       {
           Tracer( TIME );
           Serial.read();
       }
    break;

        //    Fin forzoso
    case INFO:
        Serial.println("---------------------------------------------------");
        Serial.println("Tracer v1.0 ");
        Serial.println("Simple depurador para IDE de Arduino");
        Serial.println("Estado del Sistema   ");
        Serial.print("LCD2004 counterticks ");
        Serial.println( lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.contadorticks);
        Tracer( STATUS );
        Serial.println("---------------------------------------------------");
        Serial.println("Sistema detenido ");
        FOREVER;         //  Macro en HID.h
    break;

//  Experimentales. No probados todavia
    case ESTADOBOTONERA:
        Serial.print("Botonera: " ); Serial.print( lcd2004.miBotonera.indicebuffer );
        //  Se puede imprimir el buffer, o el ultimo estado ...
    break;
    case DATA:
    //  ImprimeConsignasExternas();
    break;
  }
}



/*
    Funcion:    Ayuda()

    NOTAS: Es lo mas rapido para depurar
    En cualquier sitio donde quieras parar, incluye tan solo
    Ayuda();
*/
void Ayuda( void )
{
  Tracer( TIME );
  Tracer( SNAPSHOT );
  Tracer( STATUS );
}




/*
    Funcion:    Mofeta()

    NOTAS: Sirve para depurar
    Emplea el shield ArduinoTrainer de HispalisRobIoTics
    Este shield integra 3 LEDs en los pines 3 , 5 , 6
    Incluye Blueetooth integrado
    El shield Hercules incluye ademas un ESP 01

    Obviamente la funcion Mofeta() se emplea cdo algo huele mal :-D
*/
void Mofeta( void)
{
  if( lcd2004.pantalla.modoEdicion )
    digitalWrite( 6 , HIGH );
  else
    digitalWrite( 6 , LOW );

  if( lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.flagConsignas )
    digitalWrite( 5 , HIGH );
  else
    digitalWrite( 5 , LOW );

if( lcd2004.miBotonera.miEncoder.valoractual)
  //if( lcd2004.miMaquinaEstado.miRegistroEstado.miRelojInterno.flagEncoder )
    digitalWrite( 3 , HIGH );
  else
    digitalWrite( 3 , LOW );
}






/*
    Funcion:  MideProceso( void )

    Funciones para testar la duracion de un metodo o proceso

    NOTA: Llamar desde loop() directamente
*/
void MideProcesoenMicros( void )
{
    unsigned long int timeinicio , timefin;

    //    Proceso
    timeinicio = micros();

      //  Aqui se teclea la funcion a testar. Ejemplos -->

    timefin = micros();

    Serial.print("Diferencia .... ");
    Serial.print(timefin - timeinicio);
    Serial.print("  microsegundos " );
    FOREVER;          //    <----       for(;;);
}






/*
    Funcion:   SetConsignasExternas( void)

        Establece los datos con los que se testea el prototipo

    NOTA: Se llama en setup()
*/
void SetConsignasExternas( void )
{
  misDatosExternos.volumenminimo              = 100;
  misDatosExternos.volumenmaximo              = 900;
  misDatosExternos.volumenconsigna            = 380;
  misDatosExternos.volumenactual              = 305;

  misDatosExternos.presionPICOminimo         =   9;
  misDatosExternos.presionPICOmaximo         =  35;
  misDatosExternos.presionPICOconsigna       =  28;
  misDatosExternos.presionPICOactual         =  20;

  misDatosExternos.presionPEEPminimo         =   4;
  misDatosExternos.presionPEEPmaximo         =  65;
  misDatosExternos.presionPEEPconsigna       =  40;
  misDatosExternos.presionPEEPactual         =  55;

  misDatosExternos.frecRPMminimo             =   3;
  misDatosExternos.frecRPMmaximo             =  30;
  misDatosExternos.frecRPMconsigna           =  12;
  misDatosExternos.frecRPMactual             =  15;

  misDatosExternos.valorTRIGGER              =  SI;
  misDatosExternos.bateriaLOW                =  20;

  //  Flags de Alarma ( Establecidos en otro proceso )
  misDatosExternos.flagAlarmaRPM             =  1;
  misDatosExternos.flagAlarmaPEEP            =  0;
  misDatosExternos.flagAlarmaPICO            =  1;

  misDatosExternos.flagAlarmaRPMBajo         =  1;
  misDatosExternos.flagAlarmaRPMAlto         =  1;
  misDatosExternos.flagAlarmaPICOBajo        =  1;
  misDatosExternos.flagAlarmaPICOAlto        =  1;
  misDatosExternos.flagAlarmaPEEPBajo        =  0;
  misDatosExternos.flagAlarmaPEEPAlto        =  0;
}

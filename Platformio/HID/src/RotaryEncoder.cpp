/*
    Modulo Encoder

    Metodos:
        int RotaryEncoder::LeeEncoder( void )
        int RotaryEncoder::LeeSwitch( void )

 */
#include "HID.h"
extern Hid lcd2004;
extern class Consigna misDatosExternos;

String menuItems[MENU_ITEMS]=
{
    "alrpm",
    "alRPM",
    "apico",
    "aPICO",
    "apeep",
    "aPEEP",
    "TRIGG",
    "RECRU",
};





/*
      Metodo:EncoderSet()
      Ajustamos los valores del ENcoder en funcion de la consigna

      Podemos incluso definir diferentes "durezas de ENcoder"
 */
void RotaryEncoder::Reset( int pestado )
{
    //  switchconsigna almacena el estado que inicia modo Encoder
    //   Puede ser:
    //        Un boton { RPM PICO PEEP }
    //        El valor HID_ESTADO_MENU_ALARMAS (al pulsar switch alarma la primera vez )
    //        Una opcion de menu de alarmas
    //    Se revisa en RefrescaEstado() , en el case HID_ESTADO_SWITCHALARMAPULSADO
    if( pestado != HID_ESTADO_SWITCHALARMAPULSADO )
        switchconsigna = pestado;
    switchanterior = switchactual;
    switchactual   = pestado;

    clocktimeout   = 0;
    conttimeout    = 6;
    prescaler      = 0;
    pasosmas       = 0;     //  No tocar
    pasosmenos     = 0;
    frecRefresco   = XTAL_ENCODER_REFRESCO;    //   Refrescamos la primera vez que entramos

    valorminimo    = 1;                        //   Por defecto
    valormaximo    = 100;
    incremento      = 1;

    switch( pestado)
    {
      case HID_ESTADO_SWITCHRPMPULSADO:
          valorminimo   = lcd2004.misConsignas.frecRPMminimo;
          valormaximo   = lcd2004.misConsignas.frecRPMmaximo;
          valorconsigna = valoractual = lcd2004.misConsignas.frecRPMconsigna;
      break;
      case HID_ESTADO_SWITCHPICOPULSADO:
          valorminimo   = lcd2004.misConsignas.presionPICOminimo;
          valormaximo   = lcd2004.misConsignas.presionPICOmaximo;
          valorconsigna = valoractual = lcd2004.misConsignas.presionPICOconsigna;
      break;
      case HID_ESTADO_SWITCHPEEPPULSADO:
          valorminimo   = lcd2004.misConsignas.presionPEEPminimo;
          valormaximo   = lcd2004.misConsignas.presionPEEPmaximo;
          valorconsigna = valoractual = lcd2004.misConsignas.presionPEEPconsigna;
      break;
      case HID_ESTADO_ALARMA_TRIGGER:
          valorminimo    = 0;
          valormaximo    = 1;
          valorconsigna = valoractual = lcd2004.misConsignas.valorTRIGGER;
      break;
      //  Inicio Menu()
      case HID_ESTADO_MENU_ALARMAS:
      case HID_ESTADO_SWITCHALARMAPULSADO:
        valorminimo   =  0;
        valormaximo   =  7;
        cursor        = valorconsigna = valoractual = 0;
        conttimeout   =  8;
      break;
      case HID_ESTADO_ALARMA_RPM_MINIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.frecRPMminimo;
      break;
      case HID_ESTADO_ALARMA_RPM_MAXIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.frecRPMmaximo;
      break;
      case HID_ESTADO_ALARMA_PICO_MINIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.presionPICOminimo;
      break;
      case HID_ESTADO_ALARMA_PICO_MAXIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.presionPICOmaximo;
      break;
      case HID_ESTADO_ALARMA_PEEP_MINIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.presionPEEPminimo;
      break;
      case HID_ESTADO_ALARMA_PEEP_MAXIMO:
        valorconsigna = valoractual = lcd2004.misConsignas.presionPEEPmaximo;
      break;
      case HID_ESTADO_ENCODER_TIMEOUT:
        switchanterior  = HID_ESTADO_SWITCHESOFF;
        switchactual    = HID_ESTADO_SWITCHESOFF;
        switchconsigna  = HID_ESTADO_SWITCHESOFF;
      break;
    }
}

/*
      Metodo:Switch()
        Lee el estado del encoder y devuelve un valor segun el suceso, el cual puede ser

            pulsador --->  {  SWITCHON  , SWITCHOFF }

      Notas: Requiere tecnicas de debouncing
      Obviamente un debouncing por hardware ( via condensador por ejemplo ) complementaria este Metodo
 */
int RotaryEncoder::LeeSwitch( void )
{
  //    Control del estado del switch
  if ( !digitalRead( SWITCH_ALARMAS ) )
    return BOTONERA_SWITCH_ALARMAS_PULSADO;
  else
    return SWITCH_OFF;
}





/*
      Metodo:LeeEncoder()
        Lee el estado del encoder y devuelve el estado del mismo
        Devuelve -1 si timeout

    LeeEncoder( void }
      Devuelve:
          BOTONERA_ENCODER_GIROIZDA_PULSADO
          BOTONERA_ENCODER_GIRODCHA_PULSADO
          BOTONERA_ENCODER_CENTRO_PULSADO
 */
int RotaryEncoder::LeeEncoder( void )
{
    static uint16_t state=0;

    state = (state<<1) | digitalRead(ENCODER_CLK) | 0xe000;
    if (state==0xf000)
    {
      //    Mientras se rota el encoder
      //    no hay timeout
      clocktimeout =  0;
      //prescaler = 0;
      state   =  0x0000;
      if(digitalRead(ENCODER_DT))
      {
        pasosmas++;
//        Serial.println(">>");
        if( pasosmas >  ENCODER_DUREZA_MAS )
        {
          pasosmas = 0;
          return BOTONERA_ENCODER_GIRODCHA_PULSADO;
        }
      }
      else
      {
        pasosmenos++;
//        Serial.println("<<");
        if ( pasosmenos >  ENCODER_DUREZA_MENOS )
        {
          pasosmenos = 0;
          return BOTONERA_ENCODER_GIROIZDA_PULSADO;
        }
      }
    }
    return BOTONERA_ENCODER_CENTRO_PULSADO;
  }




/*
    Metodo: ModificaValor( int valor )
    Edita la consigna mediante llamada a Encoder

    Devuelve:  HID_ESTADO_ENCODER_TIMEOUT
               HID_ESTADO_LOOP

*/
int RotaryEncoder::ModificaValor( int testado )
{
  //    Gestion de Timeout
  if( ++frecRefresco > XTAL_ENCODER_REFRESCO )
  {
    frecRefresco = 0;
    return HID_ESTADO_ENCODER_REFRESCO;
  }

  if( ++clocktimeout > ENCODERTIMEOUT  )
  {
    clocktimeout  = NO;
    //    prescaler multiplica el numero de timeouts necesarios para salir
    prescaler++;
    if( prescaler >= conttimeout )
    {
      return HID_ESTADO_ENCODER_TIMEOUT;
    }
  }
  switch( LeeEncoder() )
  {
    case BOTONERA_ENCODER_GIROIZDA_PULSADO:
      switch( testado )
      {
        //    Menu de seleccion de alarmas
        case HID_ESTADO_MENU_ALARMAS:
          if( cursor >= 1 )
            cursor--;
          else
            cursor = MENU_ITEMS-1;
        break;
        //    Edicion de Consignas
        default:
          if( valoractual > valorminimo+incremento )
              valoractual -= incremento ;
          else
              valoractual = valorminimo;
          break;
      }
    break;
    case BOTONERA_ENCODER_GIRODCHA_PULSADO:
      switch( testado )
      {
        //    Menu de seleccion de alarmas
        case HID_ESTADO_MENU_ALARMAS:
          if( cursor < MENU_ITEMS-1 )
              cursor++;
          else
              cursor=0;
        break;
        //    Edicion de Consignas

        default:
          if( valoractual < valormaximo - incremento )
            valoractual += incremento;
            else
            valoractual = valormaximo;
        break;
      }
    break;
  }
  return HID_ESTADO_LOOP;
}

/*
     Modulo: Botonera.cpp

     Contiene los metodos de la clase

            void Botonera::Reset( void )
            ESTADO_MAQUINA Botonera::GetEstadoAnterior( void )
            ESTADO_MAQUINA Botonera::MuestreaTeclado( void )
            ESTADO_MAQUINA Botonera::EstadoBotonera( void )
            ESTADO_MAQUINA Botonera::ObtenEstadoPorlotes( void )
 */
#include  "HID.h"
extern Hid lcd2004;



/*

     Metodo:   LeeBotoneras
    Lee los sensores externos

    Las entradas que se consideran de control de Hid son
      switchRPM         Controla las revoluciones del motor
      switch_PICO       Controla la Presion de Pico
      switch_PEEP       Controla la Presion PEEP
      switchAlarmas     Da acceso a las funciones extendidas y al Encoder
      EncoderAlarmas    Permite incrementar / decrementar los diferentes valores de control

    Se ha dotado a la clase de un buffer de entrada de modo que se pueden almacenar hasta BUFFER_BOTONERA bytes
    Dicho buffer se elige del modo mas sencillo, esto es un vector de bytes,que es lo minimo que almacena el micro
    Esto incrementa la memoria total empleada, pero cada byte me permite muestrear durante 1 msg,
    por lo que 256 bytes ( longitud máxima del buffer ) equivale a almacenar la entrada durante 256 msg

    Como mejora se sugiere implantar una cola circular, para lo cual remitimos a

    Estructura de Datos en C++
    Luis Joyanes Aguilar
    Lucas Sanchez

*/



/*
     Metodo:   Reset
     Inicia el buffer de la Botonera
*/
void Botonera::Reset( void )
{
  estadoactual  = 0;
  indicebuffer  = 0;
  bufferlleno   = NO;
}



/*

  Metodo:     int Botonera::DevuelveEstado( void )

              Devuelve inmediataente el estado anterior

*/
ESTADO_MAQUINA Botonera::GetEstado( void )
{
  int aux = estadoactual;
  Reset();
  return aux;
}


/*
    Metodo: ESTADO_MAQUINA Botonera::MuestreaTeclado( void )

      Devuelve:    SI Si buffer BufferLleno
                   NO Caso contrario
*/
ESTADO_MAQUINA Botonera::MuestreaTeclado( void )
{

  if( bufferlleno )
    return SI;
  bufferbotonera[indicebuffer] = NO;
  //    Control de las entradas ( switches )
  if( !digitalRead(SWITCH_RPM) )
      bufferbotonera[indicebuffer] += BOTONERA_SWITCH_RPM_PULSADO;

  //     Switch  Presion Pico
  if ( !digitalRead(SWITCH_PICO) )
      bufferbotonera[indicebuffer]  += BOTONERA_SWITCH_PICO_PULSADO;

  //    Switch  Presion PEEP
  if ( !digitalRead(SWITCH_PEEP) )
      bufferbotonera[indicebuffer] += BOTONERA_SWITCH_PEEP_PULSADO;

  //    Switch  ALARMAS
  if ( !digitalRead(SWITCH_ALARMAS) )
      bufferbotonera[indicebuffer]  += BOTONERA_SWITCH_ALARMAS_PULSADO;

  //    Actualizamos el indice si algun estado no es Cero
  if( bufferbotonera[indicebuffer] )
  {
    //  Buffer lleno --> Obtengo estado botonera y reinicio
    if( indicebuffer >= BOTONERA_FLANCO )
    {
      estadoactual = EstadoBotonera();
      bufferlleno = SI;
      return SI;
    }
    else
      indicebuffer++;
  }
  return NO;
}





ESTADO_MAQUINA Botonera::EstadoBotonera( void )
{
    static unsigned char estadoencalculo , i;
    static unsigned char sumaparcialrpm,sumaparcialpico,sumaparcialpeep,sumaparcialalar;
    static unsigned char *bufferbase;

    estadoencalculo=0 , sumaparcialrpm = sumaparcialpico = sumaparcialpeep = sumaparcialalar = 0;

  //  Los calculos lo hacemos en bucle a bucle
  //  ya que cada 50 usg tenemos una interrupcion
  //  y no he calculado cuanto se tarda en realizar un bucle
  //  para hacerlos todos en un solo bucle
  //  y presentar un codigo mas elegante

  //    Arimetica de punteros. El ultimo elemento es bufferbotonera[indicebuffer-1]
  //    Al sumar 1 al ptro , sumo 1 * sizeof( ptro ) , es decir 1 byte
  //    Esto hay que optimizarlo tratando como un campo de bits ..ya veremos
    for( i = 0  , bufferbase = bufferbotonera+indicebuffer-1; i < BOTONERA_FLANCO ; i++ , bufferbase-- )
    {
        //    Switch RPM
        sumaparcialrpm += (*bufferbase & BOTONERA_SWITCH_RPM_PULSADO)>>3;          //    & operador binario

        //    Switch PICO
        sumaparcialpico += (*bufferbase & BOTONERA_SWITCH_PICO_PULSADO)>>2;

        //    Switch PEEP
        sumaparcialpeep += (*bufferbase & BOTONERA_SWITCH_PEEP_PULSADO)>>1;

        //    Switch Alarmas
        sumaparcialalar += *bufferbase & BOTONERA_SWITCH_ALARMAS_PULSADO;
    }

    //    Decidimos el nivel ( ALTO o BAJO )

    if( sumaparcialrpm > BOTONERA_UMBRAL_ON )
        estadoencalculo += BOTONERA_SWITCH_RPM_PULSADO;
    if( sumaparcialpico > BOTONERA_UMBRAL_ON )
        estadoencalculo += BOTONERA_SWITCH_PICO_PULSADO;
    if( sumaparcialpeep > BOTONERA_UMBRAL_ON )
        estadoencalculo += BOTONERA_SWITCH_PEEP_PULSADO;
    if( sumaparcialalar > BOTONERA_UMBRAL_ON )
        estadoencalculo += BOTONERA_SWITCH_ALARMAS_PULSADO;

    return estadoencalculo;
}
